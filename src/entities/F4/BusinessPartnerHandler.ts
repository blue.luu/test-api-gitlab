import { Handler, Req, OnRead } from "cds-routing-handlers"
import cds from "@sap/cds"
/**
 * BusinessPartner Handler.
 *
 * @export
 * @class BusinessPartner
 */
@Handler("bp.service.f4.BusinessPartnerF4Service.BusinessPartner")
export class BusinessPartnerHandler {
  @OnRead()
  public async handleOnRead(@Req() req: any): Promise<void> {
    console.log("Read BusinessPartner")
    const srv = await cds.connect.to("MDG_F4")
    req.query.SELECT.from = { ref: ["MDG_F4.BUSINESSPARTNER"] }
    const tx = srv.transaction(req)
    const result = await tx.run(req.query)
    return result
  }
}
