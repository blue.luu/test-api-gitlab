export default [
  {
    userGroupID: "BP_APPROVER",
    name: "Approver Business Partner",
  },
  {
    userGroupID: "BP_REQUESTOR",
    name: "Requestor business request for Business Partner",
  },
]
