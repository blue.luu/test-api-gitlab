export default [
  {
    objectType: "BusinessPartner",
    requestType: "CREATE",
    reason: "New Business Partner",
    isActive: true,
  },
  {
    objectType: "BusinessPartner",
    requestType: "UPDATE",
    reason: "Business Partner Edit",
    isActive: true,
  },
  {
    objectType: "BusinessPartner",
    requestType: "DELETE",
    reason: "Delete Business Partner",
    isActive: true,
  },
]
