export default [
  {
    userGroupID: "BP_REQUESTOR",
    scopeID: "CreateBPRequest",
  },
  {
    userGroupID: "BP_REQUESTOR",
    scopeID: "ChangeBPRequest",
  },
  {
    userGroupID: "BP_REQUESTOR",
    scopeID: "DeleteBPRequest",
  },
  {
    userGroupID: "BP_REQUESTOR",
    scopeID: "EndUser",
  },
  {
    userGroupID: "BP_REQUESTOR",
    scopeID: "MD_BusinessPartner",
  },
  {
    userGroupID: "BP_APPROVER",
    scopeID: "ApproveBPRequest",
  },
  {
    userGroupID: "BP_APPROVER",
    scopeID: "RejectBPRequest",
  },
  {
    userGroupID: "BP_APPROVER",
    scopeID: "EndUser",
  },
  {
    userGroupID: "BP_APPROVER",
    scopeID: "MD_BusinessPartner",
  },
]
