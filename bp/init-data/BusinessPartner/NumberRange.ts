export default [
  {
    tableName: "bp.model.final.BusinessPartner",
    key1: "",
    key1Value: "",
    key2: "",
    key2Value: "",
    key3: "",
    key3Value: "",
    prefix: "BP",
    fromNumber: "0000000001",
    toNumber: "9999999999",
    currentNumber: "0000000000",
    compareKey: "objectID",
  },
]
