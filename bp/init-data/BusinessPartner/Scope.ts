export default [
  {
    scopeID: "ApproveBPRequest",
    description: "Approve Business Partner Request",
    scopeIASDisplayName: null,
    scopeIASID: null,
    texts: [
      {
        locale: "en",
        scopeID: "ApproveBPRequest",
        description: "Approve Business Partner Request",
      },
    ],
  },
  {
    scopeID: "CreateBPRequest",
    description: "Create Business Partner Request",
    scopeIASDisplayName: null,
    scopeIASID: null,
    texts: [
      {
        locale: "en",
        scopeID: "CreateBPRequest",
        description: "Create Business Partner Request",
      },
    ],
  },
  {
    scopeID: "ChangeBPRequest",
    description: "Change a Business Partner request from requestor",
    scopeIASDisplayName: null,
    scopeIASID: null,
    texts: [
      {
        locale: "en",
        scopeID: "ChangeBPRequest",
        description: "Change a Business Partner request from requestor",
      },
    ],
  },
  {
    scopeID: "DeleteBPRequest",
    description: "Delete a Business Partner",
    scopeIASDisplayName: null,
    scopeIASID: null,
    texts: [
      {
        locale: "en",
        scopeID: "DeleteBPRequest",
        description: "Delete a Business Partner",
      },
    ],
  },
  {
    scopeID: "MD_BusinessPartner",
    description: "Generic Business Partner Scope",
    scopeIASDisplayName: null,
    scopeIASID: null,
    texts: [
      {
        locale: "en",
        scopeID: "MD_BusinessPartner",
        description: "Generic Business Partner Scope",
      },
    ],
  },
  {
    scopeID: "RejectBPRequest",
    description: "Reject a Business Partner Request",
    scopeIASDisplayName: null,
    scopeIASID: null,
    texts: [
      {
        locale: "en",
        scopeID: "RejectBPRequest",
        description: "Reject a Business Partner Request",
      },
    ],
  },
]
