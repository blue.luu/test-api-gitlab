namespace bp.model.staging;
using core.common.business_entity_staging from '@simplemdg/db_common/db/common-model';
using {
      bp.model.final.AddressUsage as final_AddressUsage, bp.model.final.AddressVersion as final_AddressVersion, bp.model.final.AddressEmailAddress as final_AddressEmailAddress, bp.model.final.AddressFaxNumber as final_AddressFaxNumber, bp.model.final.AddressMobilePhoneNumber as final_AddressMobilePhoneNumber, bp.model.final.AddressPhoneNumber as final_AddressPhoneNumber, bp.model.final.AddressHomePageUrl as final_AddressHomePageUrl, bp.model.final.CustomerBranchCode as final_CustomerBranchCode, bp.model.final.CustomerClassification as final_CustomerClassification, bp.model.final.CustomerCompany as final_CustomerCompany, bp.model.final.CustomerSalesArea as final_CustomerSalesArea, bp.model.final.CustomerVAT as final_CustomerVAT, bp.model.final.SupplierBranchCode as final_SupplierBranchCode, bp.model.final.SupplierClassification as final_SupplierClassification, bp.model.final.SupplierCompany as final_SupplierCompany, bp.model.final.SupplierPurchasingOrganization as final_SupplierPurchasingOrganization, bp.model.final.SupplierVAT as final_SupplierVAT
    } from '../final/3rd-model.cds';
using {
      bp.model.staging.CustomerCharacteristic,
bp.model.staging.CustomerDunning,
bp.model.staging.CustomerHierarchy,
bp.model.staging.CustomerPayer,
bp.model.staging.CustomerPaymentTerms,
bp.model.staging.CustomerWithHoldingTax,
bp.model.staging.CustomerPartnerFunction,
bp.model.staging.CustomerSalesAreaT,
bp.model.staging.CustomerSalesAreaTax,
bp.model.staging.SupplierCharacteristic,
bp.model.staging.SupplierDunning,
bp.model.staging.SupplierPayee,
bp.model.staging.SupplierWithHoldingTax,
bp.model.staging.SupplierPartnerFunction
    } from './4th-model';
@(
      title             : '{i18n>AddressUsage}'
    ) 
entity AddressUsage : final_AddressUsage, business_entity_staging {
}
@(
      title             : '{i18n>AddressVersion}'
    ) 
entity AddressVersion : final_AddressVersion, business_entity_staging {
}
@(
      title             : '{i18n>AddressEmailAddress}'
    ) 
entity AddressEmailAddress : final_AddressEmailAddress, business_entity_staging {
}
@(
      title             : '{i18n>AddressFaxNumber}'
    ) 
entity AddressFaxNumber : final_AddressFaxNumber, business_entity_staging {
}
@(
      title             : '{i18n>AddressMobilePhoneNumber}'
    ) 
entity AddressMobilePhoneNumber : final_AddressMobilePhoneNumber, business_entity_staging {
}
@(
      title             : '{i18n>AddressPhoneNumber}'
    ) 
entity AddressPhoneNumber : final_AddressPhoneNumber, business_entity_staging {
}
@(
      title             : '{i18n>AddressHomePageUrl}'
    ) 
entity AddressHomePageUrl : final_AddressHomePageUrl, business_entity_staging {
}
@(
      title             : '{i18n>CustomerBranchCode}'
    ) 
entity CustomerBranchCode : final_CustomerBranchCode, business_entity_staging {
}
@(
      title             : '{i18n>CustomerClassification}'
    ) 
entity CustomerClassification : final_CustomerClassification, business_entity_staging {

    to_CustomerCharacteristic : Composition of many CustomerCharacteristic
    on to_CustomerCharacteristic.objectID = $self.objectID and to_CustomerCharacteristic.taskID = $self.taskID and to_CustomerCharacteristic.draftID = $self.draftID and to_CustomerCharacteristic.customer = $self.customer and to_CustomerCharacteristic.class = $self.class and to_CustomerCharacteristic.classType = $self.classType;
}
@(
      title             : '{i18n>CustomerCompany}'
    ) 
entity CustomerCompany : final_CustomerCompany, business_entity_staging {

    to_CustomerDunning : Composition of many CustomerDunning
    on to_CustomerDunning.objectID = $self.objectID and to_CustomerDunning.taskID = $self.taskID and to_CustomerDunning.draftID = $self.draftID and to_CustomerDunning.customer = $self.customer and to_CustomerDunning.companyCode = $self.companyCode;

    to_CustomerHierarchy : Composition of one CustomerHierarchy
    on to_CustomerHierarchy.objectID = $self.objectID and to_CustomerHierarchy.taskID = $self.taskID and to_CustomerHierarchy.draftID = $self.draftID and to_CustomerHierarchy.companyCode = $self.companyCode and to_CustomerHierarchy.customer = $self.customer;

    to_CustomerPayer : Composition of many CustomerPayer
    on to_CustomerPayer.objectID = $self.objectID and to_CustomerPayer.taskID = $self.taskID and to_CustomerPayer.draftID = $self.draftID and to_CustomerPayer.companyCode = $self.companyCode and to_CustomerPayer.customer = $self.customer;

    to_CustomerPaymentTerms : Composition of many CustomerPaymentTerms
    on to_CustomerPaymentTerms.objectID = $self.objectID and to_CustomerPaymentTerms.taskID = $self.taskID and to_CustomerPaymentTerms.draftID = $self.draftID and to_CustomerPaymentTerms.companyCode = $self.companyCode and to_CustomerPaymentTerms.customer = $self.customer;

    to_CustomerWithHoldingTax : Composition of many CustomerWithHoldingTax
    on to_CustomerWithHoldingTax.objectID = $self.objectID and to_CustomerWithHoldingTax.taskID = $self.taskID and to_CustomerWithHoldingTax.draftID = $self.draftID and to_CustomerWithHoldingTax.customer = $self.customer and to_CustomerWithHoldingTax.companyCode = $self.companyCode;
}
@(
      title             : '{i18n>CustomerSalesArea}'
    ) 
entity CustomerSalesArea : final_CustomerSalesArea, business_entity_staging {

    to_CustomerPartnerFunction : Composition of many CustomerPartnerFunction
    on to_CustomerPartnerFunction.objectID = $self.objectID and to_CustomerPartnerFunction.taskID = $self.taskID and to_CustomerPartnerFunction.draftID = $self.draftID and to_CustomerPartnerFunction.customer = $self.customer and to_CustomerPartnerFunction.division = $self.division and to_CustomerPartnerFunction.distributionChannel = $self.distributionChannel and to_CustomerPartnerFunction.salesOrganization = $self.salesOrganization;

    to_CustomerSalesAreaT : Composition of many CustomerSalesAreaT
    on to_CustomerSalesAreaT.objectID = $self.objectID and to_CustomerSalesAreaT.taskID = $self.taskID and to_CustomerSalesAreaT.draftID = $self.draftID and to_CustomerSalesAreaT.division = $self.division and to_CustomerSalesAreaT.distributionChannel = $self.distributionChannel and to_CustomerSalesAreaT.salesOrganization = $self.salesOrganization and to_CustomerSalesAreaT.customer = $self.customer;

    to_SalesAreaTax : Composition of many CustomerSalesAreaTax
    on to_SalesAreaTax.objectID = $self.objectID and to_SalesAreaTax.taskID = $self.taskID and to_SalesAreaTax.draftID = $self.draftID and to_SalesAreaTax.salesOrganization = $self.salesOrganization and to_SalesAreaTax.customer = $self.customer and to_SalesAreaTax.distributionChannel = $self.distributionChannel and to_SalesAreaTax.division = $self.division;
}
@(
      title             : '{i18n>CustomerVAT}'
    ) 
entity CustomerVAT : final_CustomerVAT, business_entity_staging {
}
@(
      title             : '{i18n>SupplierBranchCode}'
    ) 
entity SupplierBranchCode : final_SupplierBranchCode, business_entity_staging {
}
@(
      title             : '{i18n>SupplierClassification}'
    ) 
entity SupplierClassification : final_SupplierClassification, business_entity_staging {

    to_SupplierCharacteristic : Composition of many SupplierCharacteristic
    on to_SupplierCharacteristic.objectID = $self.objectID and to_SupplierCharacteristic.taskID = $self.taskID and to_SupplierCharacteristic.draftID = $self.draftID and to_SupplierCharacteristic.class = $self.class and to_SupplierCharacteristic.classType = $self.classType and to_SupplierCharacteristic.supplier = $self.supplier;
}
@(
      title             : '{i18n>SupplierCompany}'
    ) 
entity SupplierCompany : final_SupplierCompany, business_entity_staging {

    to_SupplierDunning : Composition of many SupplierDunning
    on to_SupplierDunning.objectID = $self.objectID and to_SupplierDunning.taskID = $self.taskID and to_SupplierDunning.draftID = $self.draftID and to_SupplierDunning.supplier = $self.supplier and to_SupplierDunning.companyCode = $self.companyCode;

    to_SupplierPayee : Composition of many SupplierPayee
    on to_SupplierPayee.objectID = $self.objectID and to_SupplierPayee.taskID = $self.taskID and to_SupplierPayee.draftID = $self.draftID and to_SupplierPayee.supplier = $self.supplier and to_SupplierPayee.companyCode = $self.companyCode;

    to_SupplierWithHoldingTax : Composition of many SupplierWithHoldingTax
    on to_SupplierWithHoldingTax.objectID = $self.objectID and to_SupplierWithHoldingTax.taskID = $self.taskID and to_SupplierWithHoldingTax.draftID = $self.draftID and to_SupplierWithHoldingTax.supplier = $self.supplier and to_SupplierWithHoldingTax.companyCode = $self.companyCode;
}
@(
      title             : '{i18n>SupplierPurchasingOrganization}'
    ) 
entity SupplierPurchasingOrganization : final_SupplierPurchasingOrganization, business_entity_staging {

    to_SupplierPartnerFunction : Composition of many SupplierPartnerFunction
    on to_SupplierPartnerFunction.objectID = $self.objectID and to_SupplierPartnerFunction.taskID = $self.taskID and to_SupplierPartnerFunction.draftID = $self.draftID and to_SupplierPartnerFunction.supplier = $self.supplier and to_SupplierPartnerFunction.purchasingOrganization = $self.purchasingOrganization;
}
@(
      title             : '{i18n>SupplierVAT}'
    ) 
entity SupplierVAT : final_SupplierVAT, business_entity_staging {
}
