namespace bp.model.staging;
using core.common.business_entity_staging from '@simplemdg/db_common/db/common-model';
using {
      bp.model.final.CustomerCharacteristic as final_CustomerCharacteristic, bp.model.final.CustomerDunning as final_CustomerDunning, bp.model.final.CustomerHierarchy as final_CustomerHierarchy, bp.model.final.CustomerPayer as final_CustomerPayer, bp.model.final.CustomerPaymentTerms as final_CustomerPaymentTerms, bp.model.final.CustomerWithHoldingTax as final_CustomerWithHoldingTax, bp.model.final.CustomerPartnerFunction as final_CustomerPartnerFunction, bp.model.final.CustomerSalesAreaT as final_CustomerSalesAreaT, bp.model.final.CustomerSalesAreaTax as final_CustomerSalesAreaTax, bp.model.final.SupplierCharacteristic as final_SupplierCharacteristic, bp.model.final.SupplierDunning as final_SupplierDunning, bp.model.final.SupplierPayee as final_SupplierPayee, bp.model.final.SupplierWithHoldingTax as final_SupplierWithHoldingTax, bp.model.final.SupplierPartnerFunction as final_SupplierPartnerFunction
    } from '../final/4th-model.cds';

@(
      title             : '{i18n>CustomerCharacteristic}'
    ) 
entity CustomerCharacteristic : final_CustomerCharacteristic, business_entity_staging {
}
@(
      title             : '{i18n>CustomerDunning}'
    ) 
entity CustomerDunning : final_CustomerDunning, business_entity_staging {
}
@(
      title             : '{i18n>CustomerHierarchy}'
    ) 
entity CustomerHierarchy : final_CustomerHierarchy, business_entity_staging {
}
@(
      title             : '{i18n>CustomerPayer}'
    ) 
entity CustomerPayer : final_CustomerPayer, business_entity_staging {
}
@(
      title             : '{i18n>CustomerPaymentTerms}'
    ) 
entity CustomerPaymentTerms : final_CustomerPaymentTerms, business_entity_staging {
}
@(
      title             : '{i18n>CustomerWithHoldingTax}'
    ) 
entity CustomerWithHoldingTax : final_CustomerWithHoldingTax, business_entity_staging {
}
@(
      title             : '{i18n>CustomerPartnerFunction}'
    ) 
entity CustomerPartnerFunction : final_CustomerPartnerFunction, business_entity_staging {
}
@(
      title             : '{i18n>CustomerSalesAreaT}'
    ) 
entity CustomerSalesAreaT : final_CustomerSalesAreaT, business_entity_staging {
}
@(
      title             : '{i18n>CustomerSalesAreaTax}'
    ) 
entity CustomerSalesAreaTax : final_CustomerSalesAreaTax, business_entity_staging {
}
@(
      title             : '{i18n>SupplierCharacteristic}'
    ) 
entity SupplierCharacteristic : final_SupplierCharacteristic, business_entity_staging {
}
@(
      title             : '{i18n>SupplierDunning}'
    ) 
entity SupplierDunning : final_SupplierDunning, business_entity_staging {
}
@(
      title             : '{i18n>SupplierPayee}'
    ) 
entity SupplierPayee : final_SupplierPayee, business_entity_staging {
}
@(
      title             : '{i18n>SupplierWithHoldingTax}'
    ) 
entity SupplierWithHoldingTax : final_SupplierWithHoldingTax, business_entity_staging {
}
@(
      title             : '{i18n>SupplierPartnerFunction}'
    ) 
entity SupplierPartnerFunction : final_SupplierPartnerFunction, business_entity_staging {
}
