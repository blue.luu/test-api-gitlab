namespace bp.model.staging;
using core.common.business_entity_staging from '@simplemdg/db_common/db/common-model';
using {
      bp.model.final.BusinessPartner as final_BusinessPartner
    } from '../final/1st-model.cds';
using {
      bp.model.staging.BusinessPartnerCreditProfile,
bp.model.staging.BusinessPartnerCreditSegment,
bp.model.staging.BusinessPartnerIdentification,
bp.model.staging.BusinessPartnerIndustry,
bp.model.staging.BusinessPartnerAddress,
bp.model.staging.BusinessPartnerBank,
bp.model.staging.BusinessPartnerCard,
bp.model.staging.BusinessPartnerRelationship,
bp.model.staging.BusinessPartnerRole,
bp.model.staging.BusinessPartnerTax,
bp.model.staging.Customer,
bp.model.staging.Supplier
    } from './2nd-model';
@(
      title             : '{i18n>BusinessPartner}'
    ) 
entity BusinessPartner : final_BusinessPartner, business_entity_staging {

    to_BPCreditProfile : Composition of one BusinessPartnerCreditProfile
    on to_BPCreditProfile.objectID = $self.objectID and to_BPCreditProfile.taskID = $self.taskID and to_BPCreditProfile.draftID = $self.draftID and to_BPCreditProfile.businessPartner = $self.businessPartner;

    to_BPCreditSegment : Composition of many BusinessPartnerCreditSegment
    on to_BPCreditSegment.objectID = $self.objectID and to_BPCreditSegment.taskID = $self.taskID and to_BPCreditSegment.draftID = $self.draftID and to_BPCreditSegment.businessPartner = $self.businessPartner;

    to_BuPaIdentification : Composition of many BusinessPartnerIdentification
    on to_BuPaIdentification.objectID = $self.objectID and to_BuPaIdentification.taskID = $self.taskID and to_BuPaIdentification.draftID = $self.draftID and to_BuPaIdentification.businessPartner = $self.businessPartner;

    to_BuPaIndustry : Composition of many BusinessPartnerIndustry
    on to_BuPaIndustry.objectID = $self.objectID and to_BuPaIndustry.taskID = $self.taskID and to_BuPaIndustry.draftID = $self.draftID and to_BuPaIndustry.businessPartner = $self.businessPartner;

    to_BusinessPartnerAddress : Composition of many BusinessPartnerAddress
    on to_BusinessPartnerAddress.objectID = $self.objectID and to_BusinessPartnerAddress.taskID = $self.taskID and to_BusinessPartnerAddress.draftID = $self.draftID and to_BusinessPartnerAddress.businessPartner = $self.businessPartner;

    to_BusinessPartnerBank : Composition of many BusinessPartnerBank
    on to_BusinessPartnerBank.objectID = $self.objectID and to_BusinessPartnerBank.taskID = $self.taskID and to_BusinessPartnerBank.draftID = $self.draftID and to_BusinessPartnerBank.businessPartner = $self.businessPartner;

    to_BusinessPartnerCard : Composition of many BusinessPartnerCard
    on to_BusinessPartnerCard.objectID = $self.objectID and to_BusinessPartnerCard.taskID = $self.taskID and to_BusinessPartnerCard.draftID = $self.draftID and to_BusinessPartnerCard.businessPartner = $self.businessPartner;

    to_BusinessPartnerRelationship : Composition of many BusinessPartnerRelationship
    on to_BusinessPartnerRelationship.objectID = $self.objectID and to_BusinessPartnerRelationship.taskID = $self.taskID and to_BusinessPartnerRelationship.draftID = $self.draftID and to_BusinessPartnerRelationship.businessPartner = $self.businessPartner;

    to_BusinessPartnerRole : Composition of many BusinessPartnerRole
    on to_BusinessPartnerRole.objectID = $self.objectID and to_BusinessPartnerRole.taskID = $self.taskID and to_BusinessPartnerRole.draftID = $self.draftID and to_BusinessPartnerRole.businessPartner = $self.businessPartner;

    to_BusinessPartnerTax : Composition of many BusinessPartnerTax
    on to_BusinessPartnerTax.objectID = $self.objectID and to_BusinessPartnerTax.taskID = $self.taskID and to_BusinessPartnerTax.draftID = $self.draftID and to_BusinessPartnerTax.businessPartner = $self.businessPartner;

    to_Customer : Composition of one Customer
    on to_Customer.objectID = $self.objectID and to_Customer.taskID = $self.taskID and to_Customer.draftID = $self.draftID and to_Customer.customer = $self.businessPartner;

    to_Supplier : Composition of one Supplier
    on to_Supplier.objectID = $self.objectID and to_Supplier.taskID = $self.taskID and to_Supplier.draftID = $self.draftID and to_Supplier.supplier = $self.businessPartner;
}
