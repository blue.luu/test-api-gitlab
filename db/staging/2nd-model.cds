namespace bp.model.staging;
using core.common.business_entity_staging from '@simplemdg/db_common/db/common-model';
using {
      bp.model.final.BusinessPartnerCreditProfile as final_BusinessPartnerCreditProfile, bp.model.final.BusinessPartnerCreditSegment as final_BusinessPartnerCreditSegment, bp.model.final.BusinessPartnerIdentification as final_BusinessPartnerIdentification, bp.model.final.BusinessPartnerIndustry as final_BusinessPartnerIndustry, bp.model.final.BusinessPartnerAddress as final_BusinessPartnerAddress, bp.model.final.BusinessPartnerBank as final_BusinessPartnerBank, bp.model.final.BusinessPartnerCard as final_BusinessPartnerCard, bp.model.final.BusinessPartnerRelationship as final_BusinessPartnerRelationship, bp.model.final.BusinessPartnerRole as final_BusinessPartnerRole, bp.model.final.BusinessPartnerTax as final_BusinessPartnerTax, bp.model.final.Customer as final_Customer, bp.model.final.Supplier as final_Supplier
    } from '../final/2nd-model.cds';
using {
      bp.model.staging.AddressUsage,
bp.model.staging.AddressVersion,
bp.model.staging.AddressEmailAddress,
bp.model.staging.AddressFaxNumber,
bp.model.staging.AddressMobilePhoneNumber,
bp.model.staging.AddressPhoneNumber,
bp.model.staging.AddressHomePageUrl,
bp.model.staging.CustomerBranchCode,
bp.model.staging.CustomerClassification,
bp.model.staging.CustomerCompany,
bp.model.staging.CustomerSalesArea,
bp.model.staging.CustomerVAT,
bp.model.staging.SupplierBranchCode,
bp.model.staging.SupplierClassification,
bp.model.staging.SupplierCompany,
bp.model.staging.SupplierPurchasingOrganization,
bp.model.staging.SupplierVAT
    } from './3rd-model';
@(
      title             : '{i18n>BusinessPartnerCreditProfile}'
    ) 
entity BusinessPartnerCreditProfile : final_BusinessPartnerCreditProfile, business_entity_staging {
}
@(
      title             : '{i18n>BusinessPartnerCreditSegment}'
    ) 
entity BusinessPartnerCreditSegment : final_BusinessPartnerCreditSegment, business_entity_staging {
}
@(
      title             : '{i18n>BusinessPartnerIdentification}'
    ) 
entity BusinessPartnerIdentification : final_BusinessPartnerIdentification, business_entity_staging {
}
@(
      title             : '{i18n>BusinessPartnerIndustry}'
    ) 
entity BusinessPartnerIndustry : final_BusinessPartnerIndustry, business_entity_staging {
}
@(
      title             : '{i18n>BusinessPartnerAddress}'
    ) 
entity BusinessPartnerAddress : final_BusinessPartnerAddress, business_entity_staging {

    to_AddressUsage : Composition of many AddressUsage
    on to_AddressUsage.objectID = $self.objectID and to_AddressUsage.taskID = $self.taskID and to_AddressUsage.draftID = $self.draftID and to_AddressUsage.addressID = $self.addressID and to_AddressUsage.businessPartner = $self.businessPartner;

    to_AddressVersion : Composition of many AddressVersion
    on to_AddressVersion.objectID = $self.objectID and to_AddressVersion.taskID = $self.taskID and to_AddressVersion.draftID = $self.draftID and to_AddressVersion.addressID = $self.addressID;

    to_EmailAddress : Composition of many AddressEmailAddress
    on to_EmailAddress.objectID = $self.objectID and to_EmailAddress.taskID = $self.taskID and to_EmailAddress.draftID = $self.draftID and to_EmailAddress.addressID = $self.addressID;

    to_FaxNumber : Composition of many AddressFaxNumber
    on to_FaxNumber.objectID = $self.objectID and to_FaxNumber.taskID = $self.taskID and to_FaxNumber.draftID = $self.draftID and to_FaxNumber.addressID = $self.addressID;

    to_MobilePhoneNumber : Composition of many AddressMobilePhoneNumber
    on to_MobilePhoneNumber.objectID = $self.objectID and to_MobilePhoneNumber.taskID = $self.taskID and to_MobilePhoneNumber.draftID = $self.draftID and to_MobilePhoneNumber.addressID = $self.addressID;

    to_PhoneNumber : Composition of many AddressPhoneNumber
    on to_PhoneNumber.objectID = $self.objectID and to_PhoneNumber.taskID = $self.taskID and to_PhoneNumber.draftID = $self.draftID and to_PhoneNumber.addressID = $self.addressID;

    to_URLAddress : Composition of many AddressHomePageUrl
    on to_URLAddress.objectID = $self.objectID and to_URLAddress.taskID = $self.taskID and to_URLAddress.draftID = $self.draftID and to_URLAddress.addressID = $self.addressID;
}
@(
      title             : '{i18n>BusinessPartnerBank}'
    ) 
entity BusinessPartnerBank : final_BusinessPartnerBank, business_entity_staging {
}
@(
      title             : '{i18n>BusinessPartnerCard}'
    ) 
entity BusinessPartnerCard : final_BusinessPartnerCard, business_entity_staging {
}
@(
      title             : '{i18n>BusinessPartnerRelationship}'
    ) 
entity BusinessPartnerRelationship : final_BusinessPartnerRelationship, business_entity_staging {
}
@(
      title             : '{i18n>BusinessPartnerRole}'
    ) 
entity BusinessPartnerRole : final_BusinessPartnerRole, business_entity_staging {
}
@(
      title             : '{i18n>BusinessPartnerTax}'
    ) 
entity BusinessPartnerTax : final_BusinessPartnerTax, business_entity_staging {
}
@(
      title             : '{i18n>Customer}'
    ) 
entity Customer : final_Customer, business_entity_staging {

    to_CustomerBranchCode : Composition of many CustomerBranchCode
    on to_CustomerBranchCode.objectID = $self.objectID and to_CustomerBranchCode.taskID = $self.taskID and to_CustomerBranchCode.draftID = $self.draftID and to_CustomerBranchCode.customer = $self.customer;

    to_CustomerClassification : Composition of many CustomerClassification
    on to_CustomerClassification.objectID = $self.objectID and to_CustomerClassification.taskID = $self.taskID and to_CustomerClassification.draftID = $self.draftID and to_CustomerClassification.customer = $self.customer;

    to_CustomerCompany : Composition of many CustomerCompany
    on to_CustomerCompany.objectID = $self.objectID and to_CustomerCompany.taskID = $self.taskID and to_CustomerCompany.draftID = $self.draftID and to_CustomerCompany.customer = $self.customer;

    to_CustomerSalesArea : Composition of many CustomerSalesArea
    on to_CustomerSalesArea.objectID = $self.objectID and to_CustomerSalesArea.taskID = $self.taskID and to_CustomerSalesArea.draftID = $self.draftID and to_CustomerSalesArea.customer = $self.customer;

    to_CustomerVAT : Composition of many CustomerVAT
    on to_CustomerVAT.objectID = $self.objectID and to_CustomerVAT.taskID = $self.taskID and to_CustomerVAT.draftID = $self.draftID and to_CustomerVAT.customer = $self.customer;
}
@(
      title             : '{i18n>Supplier}'
    ) 
entity Supplier : final_Supplier, business_entity_staging {

    to_SupplierBranchCode : Composition of many SupplierBranchCode
    on to_SupplierBranchCode.objectID = $self.objectID and to_SupplierBranchCode.taskID = $self.taskID and to_SupplierBranchCode.draftID = $self.draftID and to_SupplierBranchCode.supplier = $self.supplier;

    to_SupplierClassification : Composition of many SupplierClassification
    on to_SupplierClassification.objectID = $self.objectID and to_SupplierClassification.taskID = $self.taskID and to_SupplierClassification.draftID = $self.draftID and to_SupplierClassification.supplier = $self.supplier;

    to_SupplierCompany : Composition of many SupplierCompany
    on to_SupplierCompany.objectID = $self.objectID and to_SupplierCompany.taskID = $self.taskID and to_SupplierCompany.draftID = $self.draftID and to_SupplierCompany.supplier = $self.supplier;

    to_SupplierPurchasingOrg : Composition of many SupplierPurchasingOrganization
    on to_SupplierPurchasingOrg.objectID = $self.objectID and to_SupplierPurchasingOrg.taskID = $self.taskID and to_SupplierPurchasingOrg.draftID = $self.draftID and to_SupplierPurchasingOrg.supplier = $self.supplier;

    to_SupplierVAT : Composition of many SupplierVAT
    on to_SupplierVAT.objectID = $self.objectID and to_SupplierVAT.taskID = $self.taskID and to_SupplierVAT.draftID = $self.draftID and to_SupplierVAT.supplier = $self.supplier;
}
