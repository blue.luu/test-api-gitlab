namespace bp.model.final;

using core.common.business_child_level_entity from '@simplemdg/db_common/db/common-model';
@(
      title             : '{i18n>CustomerCharacteristic}'
    ) 
entity CustomerCharacteristic : business_child_level_entity {
key customer : String(10) @(title : '{i18n>CustomerCharacteristic.customer}');
key class : String(18) @(title : '{i18n>CustomerCharacteristic.class}');
key classType : String(3) @(title : '{i18n>CustomerCharacteristic.classType}');
key charc : String(30) @(title : '{i18n>CustomerCharacteristic.charc}');
key counter : String(3) @(title : '{i18n>CustomerCharacteristic.counter}');
charcValue : String(70) @(title : '{i18n>CustomerCharacteristic.charcValue}');
}
@(
      title             : '{i18n>CustomerDunning}'
    ) 
entity CustomerDunning : business_child_level_entity {
key customer : String(10) @(title : '{i18n>CustomerDunning.customer}');
key companyCode : String(4) @(title : '{i18n>CustomerDunning.companyCode}');
key dunningArea : String(2) @(title : '{i18n>CustomerDunning.dunningArea}');
dunningBlock : String(1) @(title : '{i18n>CustomerDunning.dunningBlock}');
dunningLevel : String(1) @(title : '{i18n>CustomerDunning.dunningLevel}');
dunningProcedure : String(4) @(title : '{i18n>CustomerDunning.dunningProcedure}');
dunningRecipient : String(10) @(title : '{i18n>CustomerDunning.dunningRecipient}');
lastDunnedOn : Date @(title : '{i18n>CustomerDunning.lastDunnedOn}');
legDunningProcedureOn : Date @(title : '{i18n>CustomerDunning.legDunningProcedureOn}');
dunningClerk : String(2) @(title : '{i18n>CustomerDunning.dunningClerk}');
}
@(
      title             : '{i18n>CustomerHierarchy}'
    ) 
entity CustomerHierarchy : business_child_level_entity {
key customer : String(10) @(title : '{i18n>CustomerHierarchy.customer}');
key companyCode : String(4) @(title : '{i18n>CustomerHierarchy.companyCode}');
iSRole : String(1) @(title : '{i18n>CustomerHierarchy.iSRole}');
invSumRecipt : String(10) @(title : '{i18n>CustomerHierarchy.invSumRecipt}');
invSumPayer : String(10) @(title : '{i18n>CustomerHierarchy.invSumPayer}');
noRangePI : String(2) @(title : '{i18n>CustomerHierarchy.noRangePI}');
noRangeMI : String(2) @(title : '{i18n>CustomerHierarchy.noRangeMI}');
calcTaxInMI : String(1) @(title : '{i18n>CustomerHierarchy.calcTaxInMI}');
moveDueDate : String(1) @(title : '{i18n>CustomerHierarchy.moveDueDate}');
bankChrgPayee : String(1) @(title : '{i18n>CustomerHierarchy.bankChrgPayee}');
bankChargePID : String(2) @(title : '{i18n>CustomerHierarchy.bankChargePID}');
bankCountry : String(3) @(title : '{i18n>CustomerHierarchy.bankCountry}');
bankKey : String(15) @(title : '{i18n>CustomerHierarchy.bankKey}');
virtAcctNr : String(10) @(title : '{i18n>CustomerHierarchy.virtAcctNr}');
rulePayterm : String(1) @(title : '{i18n>CustomerHierarchy.rulePayterm}');
specCasesRel : String(1) @(title : '{i18n>CustomerHierarchy.specCasesRel}');
}
@(
      title             : '{i18n>CustomerPayer}'
    ) 
entity CustomerPayer : business_child_level_entity {
key customer : String(10) @(title : '{i18n>CustomerPayer.customer}');
key companyCode : String(4) @(title : '{i18n>CustomerPayer.companyCode}');
key sequenceNo : String(3) @(title : '{i18n>CustomerPayer.sequenceNo}');
alternativeName : String(48) @(title : '{i18n>CustomerPayer.alternativeName}');
counterBankName : String(15) @(title : '{i18n>CustomerPayer.counterBankName}');
counterBranch : String(15) @(title : '{i18n>CustomerPayer.counterBranch}');
}
@(
      title             : '{i18n>CustomerPaymentTerms}'
    ) 
entity CustomerPaymentTerms : business_child_level_entity {
key customer : String(10) @(title : '{i18n>CustomerPaymentTerms.customer}');
key companyCode : String(4) @(title : '{i18n>CustomerPaymentTerms.companyCode}');
key validFrom : Date @(title : '{i18n>CustomerPaymentTerms.validFrom}');
paymentTerms : String(4) @(title : '{i18n>CustomerPaymentTerms.paymentTerms}');
}
@(
      title             : '{i18n>CustomerWithHoldingTax}'
    ) 
entity CustomerWithHoldingTax : business_child_level_entity {
key customer : String(10) @(title : '{i18n>CustomerWithHoldingTax.customer}');
key companyCode : String(4) @(title : '{i18n>CustomerWithHoldingTax.companyCode}');
key withholdingTaxType : String(2) @(title : '{i18n>CustomerWithHoldingTax.withholdingTaxType}');
withholdingTaxCode : String(2) @(title : '{i18n>CustomerWithHoldingTax.withholdingTaxCode}');
withholdingTaxAgent : Boolean default false @(title : '{i18n>CustomerWithHoldingTax.withholdingTaxAgent}');
obligationDateBegin : Date @(title : '{i18n>CustomerWithHoldingTax.obligationDateBegin}');
obligationDateEnd : Date @(title : '{i18n>CustomerWithHoldingTax.obligationDateEnd}');
withholdingTaxNumber : String(16) @(title : '{i18n>CustomerWithHoldingTax.withholdingTaxNumber}');
withholdingTaxCertificate : String(25) @(title : '{i18n>CustomerWithHoldingTax.withholdingTaxCertificate}');
withholdingTaxExmptPercent : Decimal(5, 2) @(title : '{i18n>CustomerWithHoldingTax.withholdingTaxExmptPercent}');
exemptionDateBegin : Date @(title : '{i18n>CustomerWithHoldingTax.exemptionDateBegin}');
exemptionDateEnd : Date @(title : '{i18n>CustomerWithHoldingTax.exemptionDateEnd}');
exemptionReason : String(2) @(title : '{i18n>CustomerWithHoldingTax.exemptionReason}');
}
@(
      title             : '{i18n>CustomerPartnerFunction}'
    ) 
entity CustomerPartnerFunction : business_child_level_entity {
key customer : String(10) @(title : '{i18n>CustomerPartnerFunction.customer}');
key salesOrganization : String(4) @(title : '{i18n>CustomerPartnerFunction.salesOrganization}');
key distributionChannel : String(2) @(title : '{i18n>CustomerPartnerFunction.distributionChannel}');
key division : String(2) @(title : '{i18n>CustomerPartnerFunction.division}');
key partnerCounter : String(3) @(title : '{i18n>CustomerPartnerFunction.partnerCounter}');
key partnerFunction : String(2) @(title : '{i18n>CustomerPartnerFunction.partnerFunction}');
bpCustomerNumber : String(10) @(title : '{i18n>CustomerPartnerFunction.bpCustomerNumber}');
customerPartnerDescription : String(30) @(title : '{i18n>CustomerPartnerFunction.customerPartnerDescription}');
defaultPartner : Boolean default false @(title : '{i18n>CustomerPartnerFunction.defaultPartner}');
authorizationGroup : String(4) @(title : '{i18n>CustomerPartnerFunction.authorizationGroup}');
}
@(
      title             : '{i18n>CustomerSalesAreaT}'
    ) 
entity CustomerSalesAreaT : business_child_level_entity {
key customer : String(10) @(title : '{i18n>CustomerSalesAreaT.customer}');
key salesOrganization : String(4) @(title : '{i18n>CustomerSalesAreaT.salesOrganization}');
key distributionChannel : String(2) @(title : '{i18n>CustomerSalesAreaT.distributionChannel}');
key division : String(2) @(title : '{i18n>CustomerSalesAreaT.division}');
key textID : String(4) @(title : '{i18n>CustomerSalesAreaT.textID}');
key languageKey : String(2) @(title : '{i18n>CustomerSalesAreaT.languageKey}');
longText : LargeString @(title : '{i18n>CustomerSalesAreaT.longText}');
}
@(
      title             : '{i18n>CustomerSalesAreaTax}'
    ) 
entity CustomerSalesAreaTax : business_child_level_entity {
key customer : String(10) @(title : '{i18n>CustomerSalesAreaTax.customer}');
key salesOrganization : String(4) @(title : '{i18n>CustomerSalesAreaTax.salesOrganization}');
key distributionChannel : String(2) @(title : '{i18n>CustomerSalesAreaTax.distributionChannel}');
key division : String(2) @(title : '{i18n>CustomerSalesAreaTax.division}');
key departureCountry : String(3) @(title : '{i18n>CustomerSalesAreaTax.departureCountry}');
key customerTaxCategory : String(4) @(title : '{i18n>CustomerSalesAreaTax.customerTaxCategory}');
customerTaxClassification : String(1) @(title : '{i18n>CustomerSalesAreaTax.customerTaxClassification}');
}
@(
      title             : '{i18n>SupplierCharacteristic}'
    ) 
entity SupplierCharacteristic : business_child_level_entity {
key supplier : String(10) @(title : '{i18n>SupplierCharacteristic.supplier}');
key class : String(18) @(title : '{i18n>SupplierCharacteristic.class}');
key classType : String(3) @(title : '{i18n>SupplierCharacteristic.classType}');
key charc : String(30) @(title : '{i18n>SupplierCharacteristic.charc}');
key counter : String(3) @(title : '{i18n>SupplierCharacteristic.counter}');
charcValue : String(70) @(title : '{i18n>SupplierCharacteristic.charcValue}');
}
@(
      title             : '{i18n>SupplierDunning}'
    ) 
entity SupplierDunning : business_child_level_entity {
key supplier : String(10) @(title : '{i18n>SupplierDunning.supplier}');
key companyCode : String(4) @(title : '{i18n>SupplierDunning.companyCode}');
key dunningArea : String(2) @(title : '{i18n>SupplierDunning.dunningArea}');
dunningBlock : String(1) @(title : '{i18n>SupplierDunning.dunningBlock}');
dunningLevel : String(1) @(title : '{i18n>SupplierDunning.dunningLevel}');
dunningProcedure : String(4) @(title : '{i18n>SupplierDunning.dunningProcedure}');
dunningRecipient : String(10) @(title : '{i18n>SupplierDunning.dunningRecipient}');
lastDunnedOn : Date @(title : '{i18n>SupplierDunning.lastDunnedOn}');
legDunningProcedureOn : Date @(title : '{i18n>SupplierDunning.legDunningProcedureOn}');
dunningClerk : String(2) @(title : '{i18n>SupplierDunning.dunningClerk}');
}
@(
      title             : '{i18n>SupplierPayee}'
    ) 
entity SupplierPayee : business_child_level_entity {
key supplier : String(10) @(title : '{i18n>SupplierPayee.supplier}');
key companyCode : String(4) @(title : '{i18n>SupplierPayee.companyCode}');
key payee : String(10) @(title : '{i18n>SupplierPayee.payee}');
}
@(
      title             : '{i18n>SupplierWithHoldingTax}'
    ) 
entity SupplierWithHoldingTax : business_child_level_entity {
key supplier : String(10) @(title : '{i18n>SupplierWithHoldingTax.supplier}');
key companyCode : String(4) @(title : '{i18n>SupplierWithHoldingTax.companyCode}');
key withholdingTaxType : String(2) @(title : '{i18n>SupplierWithHoldingTax.withholdingTaxType}');
exemptionDateBegin : Date @(title : '{i18n>SupplierWithHoldingTax.exemptionDateBegin}');
exemptionDateEnd : Date @(title : '{i18n>SupplierWithHoldingTax.exemptionDateEnd}');
exemptionReason : String(2) @(title : '{i18n>SupplierWithHoldingTax.exemptionReason}');
isWithholdingTaxSubject : Boolean default false @(title : '{i18n>SupplierWithHoldingTax.isWithholdingTaxSubject}');
recipientType : String(2) @(title : '{i18n>SupplierWithHoldingTax.recipientType}');
withholdingTaxCertificate : String(25) @(title : '{i18n>SupplierWithHoldingTax.withholdingTaxCertificate}');
withholdingTaxCode : String(2) @(title : '{i18n>SupplierWithHoldingTax.withholdingTaxCode}');
withholdingTaxExmptPercent : Decimal(5, 2) @(title : '{i18n>SupplierWithHoldingTax.withholdingTaxExmptPercent}');
withholdingTaxNumber : String(16) @(title : '{i18n>SupplierWithHoldingTax.withholdingTaxNumber}');
}
@(
      title             : '{i18n>SupplierPartnerFunction}'
    ) 
entity SupplierPartnerFunction : business_child_level_entity {
key supplier : String(10) @(title : '{i18n>SupplierPartnerFunction.supplier}');
key purchasingOrganization : String(4) @(title : '{i18n>SupplierPartnerFunction.purchasingOrganization}');
key supplierSubrange : String(6) @(title : '{i18n>SupplierPartnerFunction.supplierSubrange}');
key plant : String(4) @(title : '{i18n>SupplierPartnerFunction.plant}');
key partnerFunction : String(2) @(title : '{i18n>SupplierPartnerFunction.partnerFunction}');
key partnerCounter : String(3) @(title : '{i18n>SupplierPartnerFunction.partnerCounter}');
defaultPartner : Boolean default false @(title : '{i18n>SupplierPartnerFunction.defaultPartner}');
creationDate : Date @(title : '{i18n>SupplierPartnerFunction.creationDate}');
createdByUser : String(12) @(title : '{i18n>SupplierPartnerFunction.createdByUser}');
referenceSupplier : String(10) @(title : '{i18n>SupplierPartnerFunction.referenceSupplier}');
authorizationGroup : String(4) @(title : '{i18n>SupplierPartnerFunction.authorizationGroup}');
}
