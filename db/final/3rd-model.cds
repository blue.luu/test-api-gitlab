namespace bp.model.final;
using {
      bp.model.final.CustomerCharacteristic,
bp.model.final.CustomerDunning,
bp.model.final.CustomerHierarchy,
bp.model.final.CustomerPayer,
bp.model.final.CustomerPaymentTerms,
bp.model.final.CustomerWithHoldingTax,
bp.model.final.CustomerPartnerFunction,
bp.model.final.CustomerSalesAreaT,
bp.model.final.CustomerSalesAreaTax,
bp.model.final.SupplierCharacteristic,
bp.model.final.SupplierDunning,
bp.model.final.SupplierPayee,
bp.model.final.SupplierWithHoldingTax,
bp.model.final.SupplierPartnerFunction
    } from './4th-model';
using core.common.business_child_level_entity from '@simplemdg/db_common/db/common-model';
@(
      title             : '{i18n>AddressUsage}'
    ) 
entity AddressUsage : business_child_level_entity {
key businessPartner : String(10) @(title : '{i18n>AddressUsage.businessPartner}');
key validityEndDate : DateTime @(title : '{i18n>AddressUsage.validityEndDate}');
key addressUsage : String(10) @(title : '{i18n>AddressUsage.addressUsage}');
key addressID : String(10) @(title : '{i18n>AddressUsage.addressID}');
validityStartDate : DateTime @(title : '{i18n>AddressUsage.validityStartDate}');
standardUsage : Boolean default false @(title : '{i18n>AddressUsage.standardUsage}');
authorizationGroup : String(4) @(title : '{i18n>AddressUsage.authorizationGroup}');
}
@(
      title             : '{i18n>AddressVersion}'
    ) 
entity AddressVersion : business_child_level_entity {
key addressID : String(10) @(title : '{i18n>AddressVersion.addressID}');
key addressVersion : String(1) @(title : '{i18n>AddressVersion.addressVersion}');
street : String(60) @(title : '{i18n>AddressVersion.street}');
houseNumber : String(10) @(title : '{i18n>AddressVersion.houseNumber}');
city : String(40) @(title : '{i18n>AddressVersion.city}');
searchTerm1OrgGrp : String(20) @(title : '{i18n>AddressVersion.searchTerm1OrgGrp}');
searchTerm2OrgGrp : String(20) @(title : '{i18n>AddressVersion.searchTerm2OrgGrp}');
name : String(40) @(title : '{i18n>AddressVersion.name}');
name2 : String(40) @(title : '{i18n>AddressVersion.name2}');
name3 : String(40) @(title : '{i18n>AddressVersion.name3}');
name4 : String(40) @(title : '{i18n>AddressVersion.name4}');
building : String(20) @(title : '{i18n>AddressVersion.building}');
room : String(10) @(title : '{i18n>AddressVersion.room}');
floor : String(10) @(title : '{i18n>AddressVersion.floor}');
street2 : String(40) @(title : '{i18n>AddressVersion.street2}');
street3 : String(40) @(title : '{i18n>AddressVersion.street3}');
street4 : String(40) @(title : '{i18n>AddressVersion.street4}');
searchTerm1Prs : String(20) @(title : '{i18n>AddressVersion.searchTerm1Prs}');
searchTerm2Prs : String(20) @(title : '{i18n>AddressVersion.searchTerm2Prs}');
title : String(4) @(title : '{i18n>AddressVersion.title}');
firstName : String(40) @(title : '{i18n>AddressVersion.firstName}');
lastName : String(40) @(title : '{i18n>AddressVersion.lastName}');
birthName : String(40) @(title : '{i18n>AddressVersion.birthName}');
middleName : String(40) @(title : '{i18n>AddressVersion.middleName}');
otherLastName : String(40) @(title : '{i18n>AddressVersion.otherLastName}');
fullName : String(80) @(title : '{i18n>AddressVersion.fullName}');
fullNameConvertST : String(1) @(title : '{i18n>AddressVersion.fullNameConvertST}');
academicTitle : String(4) @(title : '{i18n>AddressVersion.academicTitle}');
secondacademicTitle : String(4) @(title : '{i18n>AddressVersion.secondacademicTitle}');
namePrefix : String(4) @(title : '{i18n>AddressVersion.namePrefix}');
secondNamePrefix : String(4) @(title : '{i18n>AddressVersion.secondNamePrefix}');
nameSupplement : String(4) @(title : '{i18n>AddressVersion.nameSupplement}');
nickname : String(40) @(title : '{i18n>AddressVersion.nickname}');
middleInitial : String(10) @(title : '{i18n>AddressVersion.middleInitial}');
nameFormat : String(2) @(title : '{i18n>AddressVersion.nameFormat}');
nameCountry : String(3) @(title : '{i18n>AddressVersion.nameCountry}');
profession : String(40) @(title : '{i18n>AddressVersion.profession}');
}
@(
      title             : '{i18n>AddressEmailAddress}'
    ) 
entity AddressEmailAddress : business_child_level_entity {
key addressID : String(10) @(title : '{i18n>AddressEmailAddress.addressID}');
key person : String(10) @(title : '{i18n>AddressEmailAddress.person}');
key ordinalNumber : String(3) @(title : '{i18n>AddressEmailAddress.ordinalNumber}');
isDefaultEmailAddress : Boolean default false @(title : '{i18n>AddressEmailAddress.isDefaultEmailAddress}');
emailAddress : String(241) @(title : '{i18n>AddressEmailAddress.emailAddress}');
searchEmailAddress : String(20) @(title : '{i18n>AddressEmailAddress.searchEmailAddress}');
}
@(
      title             : '{i18n>AddressFaxNumber}'
    ) 
entity AddressFaxNumber : business_child_level_entity {
key addressID : String(10) @(title : '{i18n>AddressFaxNumber.addressID}');
key person : String(10) @(title : '{i18n>AddressFaxNumber.person}');
key ordinalNumber : String(3) @(title : '{i18n>AddressFaxNumber.ordinalNumber}');
isDefaultFaxNumber : Boolean default false @(title : '{i18n>AddressFaxNumber.isDefaultFaxNumber}');
faxCountry : String(3) @(title : '{i18n>AddressFaxNumber.faxCountry}');
faxNumber : String(30) @(title : '{i18n>AddressFaxNumber.faxNumber}');
faxNumberExtension : String(10) @(title : '{i18n>AddressFaxNumber.faxNumberExtension}');
internationalFaxNumber : String(30) @(title : '{i18n>AddressFaxNumber.internationalFaxNumber}');
}
@(
      title             : '{i18n>AddressMobilePhoneNumber}'
    ) 
entity AddressMobilePhoneNumber : business_child_level_entity {
key addressID : String(10) @(title : '{i18n>AddressMobilePhoneNumber.addressID}');
key person : String(10) @(title : '{i18n>AddressMobilePhoneNumber.person}');
key ordinalNumber : String(3) @(title : '{i18n>AddressMobilePhoneNumber.ordinalNumber}');
destinationLocationCountry : String(3) @(title : '{i18n>AddressMobilePhoneNumber.destinationLocationCountry}');
isDefaultPhoneNumber : Boolean default false @(title : '{i18n>AddressMobilePhoneNumber.isDefaultPhoneNumber}');
phoneNumber : String(30) @(title : '{i18n>AddressMobilePhoneNumber.phoneNumber}');
phoneNumberExtension : String(10) @(title : '{i18n>AddressMobilePhoneNumber.phoneNumberExtension}');
internationalPhoneNumber : String(30) @(title : '{i18n>AddressMobilePhoneNumber.internationalPhoneNumber}');
phoneNumberType : String(1) @(title : '{i18n>AddressMobilePhoneNumber.phoneNumberType}');
}
@(
      title             : '{i18n>AddressPhoneNumber}'
    ) 
entity AddressPhoneNumber : business_child_level_entity {
key addressID : String(10) @(title : '{i18n>AddressPhoneNumber.addressID}');
key person : String(10) @(title : '{i18n>AddressPhoneNumber.person}');
key ordinalNumber : String(3) @(title : '{i18n>AddressPhoneNumber.ordinalNumber}');
destinationLocationCountry : String(3) @(title : '{i18n>AddressPhoneNumber.destinationLocationCountry}');
isDefaultPhoneNumber : Boolean default false @(title : '{i18n>AddressPhoneNumber.isDefaultPhoneNumber}');
phoneNumber : String(30) @(title : '{i18n>AddressPhoneNumber.phoneNumber}');
phoneNumberExtension : String(10) @(title : '{i18n>AddressPhoneNumber.phoneNumberExtension}');
internationalPhoneNumber : String(30) @(title : '{i18n>AddressPhoneNumber.internationalPhoneNumber}');
phoneNumberType : String(1) @(title : '{i18n>AddressPhoneNumber.phoneNumberType}');
}
@(
      title             : '{i18n>AddressHomePageUrl}'
    ) 
entity AddressHomePageUrl : business_child_level_entity {
key addressID : String(10) @(title : '{i18n>AddressHomePageUrl.addressID}');
key person : String(10) @(title : '{i18n>AddressHomePageUrl.person}');
key ordinalNumber : String(3) @(title : '{i18n>AddressHomePageUrl.ordinalNumber}');
key validityStartDate : Date @(title : '{i18n>AddressHomePageUrl.validityStartDate}');
key isDefaultURLAddress : Boolean default false @(title : '{i18n>AddressHomePageUrl.isDefaultURLAddress}');
uRIType : String(3) @(title : '{i18n>AddressHomePageUrl.uRIType}');
searchURLAddress : String(50) @(title : '{i18n>AddressHomePageUrl.searchURLAddress}');
uRLFieldLength : Integer @(title : '{i18n>AddressHomePageUrl.uRLFieldLength}');
websiteURL : String(2048) @(title : '{i18n>AddressHomePageUrl.websiteURL}');
}
@(
      title             : '{i18n>CustomerBranchCode}'
    ) 
entity CustomerBranchCode : business_child_level_entity {
key customer : String(10) @(title : '{i18n>CustomerBranchCode.customer}');
key branchCode : String(5) @(title : '{i18n>CustomerBranchCode.branchCode}');
key language : String(2) @(title : '{i18n>CustomerBranchCode.language}');
description : String(40) @(title : '{i18n>CustomerBranchCode.description}');
defaultBranch : String(1) @(title : '{i18n>CustomerBranchCode.defaultBranch}');
}
@(
      title             : '{i18n>CustomerClassification}'
    ) 
entity CustomerClassification : business_child_level_entity {
key customer : String(10) @(title : '{i18n>CustomerClassification.customer}');
key class : String(18) @(title : '{i18n>CustomerClassification.class}');
key classType : String(3) @(title : '{i18n>CustomerClassification.classType}');
objectType : String(10) @(title : '{i18n>CustomerClassification.objectType}');

    to_CustomerCharacteristic : Composition of many CustomerCharacteristic
    on to_CustomerCharacteristic.objectID = $self.objectID and to_CustomerCharacteristic.customer = $self.customer and to_CustomerCharacteristic.class = $self.class and to_CustomerCharacteristic.classType = $self.classType;
}
@(
      title             : '{i18n>CustomerCompany}'
    ) 
entity CustomerCompany : business_child_level_entity {
key customer : String(10) @(title : '{i18n>CustomerCompany.customer}');
key companyCode : String(4) @(title : '{i18n>CustomerCompany.companyCode}');
aPARToleranceGroup : String(4) @(title : '{i18n>CustomerCompany.aPARToleranceGroup}');
accountByCustomer : String(12) @(title : '{i18n>CustomerCompany.accountByCustomer}');
accountingClerk : String(2) @(title : '{i18n>CustomerCompany.accountingClerk}');
accountingClerkFaxNumber : String(31) @(title : '{i18n>CustomerCompany.accountingClerkFaxNumber}');
accountingClerkInternetAddress : String(130) @(title : '{i18n>CustomerCompany.accountingClerkInternetAddress}');
accountingClerkPhoneNumber : String(30) @(title : '{i18n>CustomerCompany.accountingClerkPhoneNumber}');
alternativePayerAccount : String(10) @(title : '{i18n>CustomerCompany.alternativePayerAccount}');
authorizationGroup : String(4) @(title : '{i18n>CustomerCompany.authorizationGroup}');
collectiveInvoiceVariant : String(1) @(title : '{i18n>CustomerCompany.collectiveInvoiceVariant}');
customerAccountNote : String(30) @(title : '{i18n>CustomerCompany.customerAccountNote}');
customerHeadOffice : String(10) @(title : '{i18n>CustomerCompany.customerHeadOffice}');
customerSupplierClearingIsUsed : Boolean default false @(title : '{i18n>CustomerCompany.customerSupplierClearingIsUsed}');
houseBank : String(5) @(title : '{i18n>CustomerCompany.houseBank}');
interestCalculationCode : String(2) @(title : '{i18n>CustomerCompany.interestCalculationCode}');
interestCalculationDate : Date @(title : '{i18n>CustomerCompany.interestCalculationDate}');
intrstCalcFrequencyInMonths : String(2) @(title : '{i18n>CustomerCompany.intrstCalcFrequencyInMonths}');
isToBeLocallyProcessed : Boolean default false @(title : '{i18n>CustomerCompany.isToBeLocallyProcessed}');
itemIsToBePaidSeparately : Boolean default false @(title : '{i18n>CustomerCompany.itemIsToBePaidSeparately}');
layoutSortingRule : String(3) @(title : '{i18n>CustomerCompany.layoutSortingRule}');
paymentBlockingReason : String(1) @(title : '{i18n>CustomerCompany.paymentBlockingReason}');
paymentMethodsList : String(10) @(title : '{i18n>CustomerCompany.paymentMethodsList}');
paymentTerms : String(4) @(title : '{i18n>CustomerCompany.paymentTerms}');
paytAdviceIsSentbyEDI : Boolean default false @(title : '{i18n>CustomerCompany.paytAdviceIsSentbyEDI}');
physicalInventoryBlockInd : Boolean default false @(title : '{i18n>CustomerCompany.physicalInventoryBlockInd}');
reconciliationAccount : String(10) @(title : '{i18n>CustomerCompany.reconciliationAccount}');
recordPaymentHistoryIndicator : Boolean default false @(title : '{i18n>CustomerCompany.recordPaymentHistoryIndicator}');
userAtCustomer : String(15) @(title : '{i18n>CustomerCompany.userAtCustomer}');
deletionIndicator : Boolean default false @(title : '{i18n>CustomerCompany.deletionIndicator}');
valueAdjustmentKey : String(2) @(title : '{i18n>CustomerCompany.valueAdjustmentKey}');
lastInterestCalcRunDate : Date @(title : '{i18n>CustomerCompany.lastInterestCalcRunDate}');
custPreviousMasterRecordNumber : String(10) @(title : '{i18n>CustomerCompany.custPreviousMasterRecordNumber}');
planningGroup : String(10) @(title : '{i18n>CustomerCompany.planningGroup}');
personnelNumber : String(8) @(title : '{i18n>CustomerCompany.personnelNumber}');
accountNumberOfBuyingGroup : String(10) @(title : '{i18n>CustomerCompany.accountNumberOfBuyingGroup}');
termsOfPmntForBillOfExCharges : String(4) @(title : '{i18n>CustomerCompany.termsOfPmntForBillOfExCharges}');
probableTimeTilCheckIsEncashed : Decimal(3, 0) @(title : '{i18n>CustomerCompany.probableTimeTilCheckIsEncashed}');
shortKeyKnownNegotiatedLeave : String(4) @(title : '{i18n>CustomerCompany.shortKeyKnownNegotiatedLeave}');
keyForPaymentGrouping : String(2) @(title : '{i18n>CustomerCompany.keyForPaymentGrouping}');
billOfExchangeLimit : Decimal(14, 3) @(title : '{i18n>CustomerCompany.billOfExchangeLimit}');
lockbox : String(7) @(title : '{i18n>CustomerCompany.lockbox}');
nextPayee : String(10) @(title : '{i18n>CustomerCompany.nextPayee}');
accountsReceivablePledgingInd : String(2) @(title : '{i18n>CustomerCompany.accountsReceivablePledgingInd}');
reasonCodeConversionVersion : String(3) @(title : '{i18n>CustomerCompany.reasonCodeConversionVersion}');
selectionRuleForPaymentAdvices : String(3) @(title : '{i18n>CustomerCompany.selectionRuleForPaymentAdvices}');
pmntNoticeToCuswClearedItems : Boolean default false @(title : '{i18n>CustomerCompany.pmntNoticeToCuswClearedItems}');
pmntNoticeToCuswoClearedItems : Boolean default false @(title : '{i18n>CustomerCompany.pmntNoticeToCuswoClearedItems}');
paymentNoticeToTheAccntDeptInd : Boolean default false @(title : '{i18n>CustomerCompany.paymentNoticeToTheAccntDeptInd}');
paymentNoticeToSalesDepartment : Boolean default false @(title : '{i18n>CustomerCompany.paymentNoticeToSalesDepartment}');
paymentNoticeToLegalDepartment : Boolean default false @(title : '{i18n>CustomerCompany.paymentNoticeToLegalDepartment}');
insuranceNumber : String(10) @(title : '{i18n>CustomerCompany.insuranceNumber}');
exportCreditInsInstitutionNum : String(2) @(title : '{i18n>CustomerCompany.exportCreditInsInstitutionNum}');
amountInsured : Decimal(14, 3) @(title : '{i18n>CustomerCompany.amountInsured}');
validityDateOfInsurance : Date @(title : '{i18n>CustomerCompany.validityDateOfInsurance}');
insuranceTarget : Decimal(3, 0) @(title : '{i18n>CustomerCompany.insuranceTarget}');
percentageOfDeductible : Decimal(3, 0) @(title : '{i18n>CustomerCompany.percentageOfDeductible}');
deletionBlock : Boolean default false @(title : '{i18n>CustomerCompany.deletionBlock}');
dunningNoticeGroup : String(2) @(title : '{i18n>CustomerCompany.dunningNoticeGroup}');

    to_CustomerDunning : Composition of many CustomerDunning
    on to_CustomerDunning.objectID = $self.objectID and to_CustomerDunning.customer = $self.customer and to_CustomerDunning.companyCode = $self.companyCode;

    to_CustomerHierarchy : Composition of one CustomerHierarchy
    on to_CustomerHierarchy.objectID = $self.objectID and to_CustomerHierarchy.companyCode = $self.companyCode and to_CustomerHierarchy.customer = $self.customer;

    to_CustomerPayer : Composition of many CustomerPayer
    on to_CustomerPayer.objectID = $self.objectID and to_CustomerPayer.companyCode = $self.companyCode and to_CustomerPayer.customer = $self.customer;

    to_CustomerPaymentTerms : Composition of many CustomerPaymentTerms
    on to_CustomerPaymentTerms.objectID = $self.objectID and to_CustomerPaymentTerms.companyCode = $self.companyCode and to_CustomerPaymentTerms.customer = $self.customer;

    to_CustomerWithHoldingTax : Composition of many CustomerWithHoldingTax
    on to_CustomerWithHoldingTax.objectID = $self.objectID and to_CustomerWithHoldingTax.customer = $self.customer and to_CustomerWithHoldingTax.companyCode = $self.companyCode;
}
@(
      title             : '{i18n>CustomerSalesArea}'
    ) 
entity CustomerSalesArea : business_child_level_entity {
key customer : String(10) @(title : '{i18n>CustomerSalesArea.customer}');
key salesOrganization : String(4) @(title : '{i18n>CustomerSalesArea.salesOrganization}');
key distributionChannel : String(2) @(title : '{i18n>CustomerSalesArea.distributionChannel}');
key division : String(2) @(title : '{i18n>CustomerSalesArea.division}');
accountByCustomer : String(12) @(title : '{i18n>CustomerSalesArea.accountByCustomer}');
authorizationGroup : String(4) @(title : '{i18n>CustomerSalesArea.authorizationGroup}');
billingIsBlockedForCustomer : String(2) @(title : '{i18n>CustomerSalesArea.billingIsBlockedForCustomer}');
completeDeliveryIsDefined : Boolean default false @(title : '{i18n>CustomerSalesArea.completeDeliveryIsDefined}');
currency : String(5) @(title : '{i18n>CustomerSalesArea.currency}');
customerABCClassification : String(2) @(title : '{i18n>CustomerSalesArea.customerABCClassification}');
customerAccountAssignmentGroup : String(2) @(title : '{i18n>CustomerSalesArea.customerAccountAssignmentGroup}');
customerGroup : String(2) @(title : '{i18n>CustomerSalesArea.customerGroup}');
customerPaymentTerms : String(4) @(title : '{i18n>CustomerSalesArea.customerPaymentTerms}');
customerPriceGroup : String(2) @(title : '{i18n>CustomerSalesArea.customerPriceGroup}');
customerPricingProcedure : String(2) @(title : '{i18n>CustomerSalesArea.customerPricingProcedure}');
deliveryIsBlockedForCustomer : String(2) @(title : '{i18n>CustomerSalesArea.deliveryIsBlockedForCustomer}');
deliveryPriority : String(2) @(title : '{i18n>CustomerSalesArea.deliveryPriority}');
incotermsClassification : String(3) @(title : '{i18n>CustomerSalesArea.incotermsClassification}');
incotermsLocation2 : String(70) @(title : '{i18n>CustomerSalesArea.incotermsLocation2}');
incotermsVersion : String(4) @(title : '{i18n>CustomerSalesArea.incotermsVersion}');
incotermsLocation1 : String(70) @(title : '{i18n>CustomerSalesArea.incotermsLocation1}');
deletionIndicator : Boolean default false @(title : '{i18n>CustomerSalesArea.deletionIndicator}');
incotermsTransferLocation : String(28) @(title : '{i18n>CustomerSalesArea.incotermsTransferLocation}');
invoiceDate : String(2) @(title : '{i18n>CustomerSalesArea.invoiceDate}');
itemOrderProbabilityInPercent : String(3) @(title : '{i18n>CustomerSalesArea.itemOrderProbabilityInPercent}');
orderCombinationIsAllowed : Boolean default false @(title : '{i18n>CustomerSalesArea.orderCombinationIsAllowed}');
orderIsBlockedForCustomer : String(2) @(title : '{i18n>CustomerSalesArea.orderIsBlockedForCustomer}');
partialDeliveryIsAllowed : String(1) @(title : '{i18n>CustomerSalesArea.partialDeliveryIsAllowed}');
priceListType : String(2) @(title : '{i18n>CustomerSalesArea.priceListType}');
salesGroup : String(3) @(title : '{i18n>CustomerSalesArea.salesGroup}');
salesOffice : String(4) @(title : '{i18n>CustomerSalesArea.salesOffice}');
shippingCondition : String(2) @(title : '{i18n>CustomerSalesArea.shippingCondition}');
supplyingPlant : String(4) @(title : '{i18n>CustomerSalesArea.supplyingPlant}');
salesDistrict : String(6) @(title : '{i18n>CustomerSalesArea.salesDistrict}');
salesItemProposal : String(10) @(title : '{i18n>CustomerSalesArea.salesItemProposal}');
maxNmbrOfPartialDelivery : Decimal(1, 0) @(title : '{i18n>CustomerSalesArea.maxNmbrOfPartialDelivery}');
underdelivTolrtdLmtRatioInPct : Decimal(3, 1) @(title : '{i18n>CustomerSalesArea.underdelivTolrtdLmtRatioInPct}');
overdelivTolrtdLmtRatioInPct : Decimal(3, 1) @(title : '{i18n>CustomerSalesArea.overdelivTolrtdLmtRatioInPct}');
additionalCustomerGroup1 : String(3) @(title : '{i18n>CustomerSalesArea.additionalCustomerGroup1}');
additionalCustomerGroup2 : String(3) @(title : '{i18n>CustomerSalesArea.additionalCustomerGroup2}');
additionalCustomerGroup3 : String(3) @(title : '{i18n>CustomerSalesArea.additionalCustomerGroup3}');
additionalCustomerGroup4 : String(3) @(title : '{i18n>CustomerSalesArea.additionalCustomerGroup4}');
additionalCustomerGroup5 : String(3) @(title : '{i18n>CustomerSalesArea.additionalCustomerGroup5}');
roundingOff : Boolean default false @(title : '{i18n>CustomerSalesArea.roundingOff}');
unitOfMeasureGroup : String(4) @(title : '{i18n>CustomerSalesArea.unitOfMeasureGroup}');
ppCustomerProced : String(2) @(title : '{i18n>CustomerSalesArea.ppCustomerProced}');
exchangeRateType : String(4) @(title : '{i18n>CustomerSalesArea.exchangeRateType}');
relevantForSettlementMngmnt : Boolean default false @(title : '{i18n>CustomerSalesArea.relevantForSettlementMngmnt}');
podRelevant : Boolean default false @(title : '{i18n>CustomerSalesArea.podRelevant}');
podTimeframe : Decimal(11, 0) @(title : '{i18n>CustomerSalesArea.podTimeframe}');
unlimitedTolerance : Boolean default false @(title : '{i18n>CustomerSalesArea.unlimitedTolerance}');
susInvoiceProc : Boolean default false @(title : '{i18n>CustomerSalesArea.susInvoiceProc}');
invoiceListSched : String(2) @(title : '{i18n>CustomerSalesArea.invoiceListSched}');
paymtGuarantProc : String(4) @(title : '{i18n>CustomerSalesArea.paymtGuarantProc}');
blockSalesSupport : String(2) @(title : '{i18n>CustomerSalesArea.blockSalesSupport}');
finalPortOfDischarge : String(10) @(title : '{i18n>CustomerSalesArea.finalPortOfDischarge}');
destinationWarehouse : String(10) @(title : '{i18n>CustomerSalesArea.destinationWarehouse}');
planningChannel : String(10) @(title : '{i18n>CustomerSalesArea.planningChannel}');
serviceType : String(10) @(title : '{i18n>CustomerSalesArea.serviceType}');

    to_CustomerPartnerFunction : Composition of many CustomerPartnerFunction
    on to_CustomerPartnerFunction.objectID = $self.objectID and to_CustomerPartnerFunction.customer = $self.customer and to_CustomerPartnerFunction.division = $self.division and to_CustomerPartnerFunction.distributionChannel = $self.distributionChannel and to_CustomerPartnerFunction.salesOrganization = $self.salesOrganization;

    to_CustomerSalesAreaT : Composition of many CustomerSalesAreaT
    on to_CustomerSalesAreaT.objectID = $self.objectID and to_CustomerSalesAreaT.division = $self.division and to_CustomerSalesAreaT.distributionChannel = $self.distributionChannel and to_CustomerSalesAreaT.salesOrganization = $self.salesOrganization and to_CustomerSalesAreaT.customer = $self.customer;

    to_SalesAreaTax : Composition of many CustomerSalesAreaTax
    on to_SalesAreaTax.objectID = $self.objectID and to_SalesAreaTax.salesOrganization = $self.salesOrganization and to_SalesAreaTax.customer = $self.customer and to_SalesAreaTax.distributionChannel = $self.distributionChannel and to_SalesAreaTax.division = $self.division;
}
@(
      title             : '{i18n>CustomerVAT}'
    ) 
entity CustomerVAT : business_child_level_entity {
key customer : String(10) @(title : '{i18n>CustomerVAT.customer}');
key validDateFrom : Date @(title : '{i18n>CustomerVAT.validDateFrom}');
taxNumber1 : String(16) @(title : '{i18n>CustomerVAT.taxNumber1}');
taxNumber2 : String(11) @(title : '{i18n>CustomerVAT.taxNumber2}');
typeOfBusiness : String(100) @(title : '{i18n>CustomerVAT.typeOfBusiness}');
typeOfIndustry : String(100) @(title : '{i18n>CustomerVAT.typeOfIndustry}');
nameOfRepresentative : String(100) @(title : '{i18n>CustomerVAT.nameOfRepresentative}');
businessPlace : String(4) @(title : '{i18n>CustomerVAT.businessPlace}');
departmentName : String(100) @(title : '{i18n>CustomerVAT.departmentName}');
personName : String(100) @(title : '{i18n>CustomerVAT.personName}');
personEmail1 : String(100) @(title : '{i18n>CustomerVAT.personEmail1}');
personEmail2 : String(100) @(title : '{i18n>CustomerVAT.personEmail2}');
personPhone1 : String(100) @(title : '{i18n>CustomerVAT.personPhone1}');
personPhone2 : String(100) @(title : '{i18n>CustomerVAT.personPhone2}');
rDepartmentName2 : String(100) @(title : '{i18n>CustomerVAT.rDepartmentName2}');
rPersonname2 : String(100) @(title : '{i18n>CustomerVAT.rPersonname2}');
selfBilling : Boolean default false @(title : '{i18n>CustomerVAT.selfBilling}');
monthTax : Boolean default false @(title : '{i18n>CustomerVAT.monthTax}');
}
@(
      title             : '{i18n>SupplierBranchCode}'
    ) 
entity SupplierBranchCode : business_child_level_entity {
key supplier : String(10) @(title : '{i18n>SupplierBranchCode.supplier}');
key branchCode : String(5) @(title : '{i18n>SupplierBranchCode.branchCode}');
key language : String(2) @(title : '{i18n>SupplierBranchCode.language}');
description : String(40) @(title : '{i18n>SupplierBranchCode.description}');
defaultBranch : String(1) @(title : '{i18n>SupplierBranchCode.defaultBranch}');
}
@(
      title             : '{i18n>SupplierClassification}'
    ) 
entity SupplierClassification : business_child_level_entity {
key supplier : String(10) @(title : '{i18n>SupplierClassification.supplier}');
key class : String(18) @(title : '{i18n>SupplierClassification.class}');
key classType : String(3) @(title : '{i18n>SupplierClassification.classType}');
objectType : String(10) @(title : '{i18n>SupplierClassification.objectType}');

    to_SupplierCharacteristic : Composition of many SupplierCharacteristic
    on to_SupplierCharacteristic.objectID = $self.objectID and to_SupplierCharacteristic.class = $self.class and to_SupplierCharacteristic.classType = $self.classType and to_SupplierCharacteristic.supplier = $self.supplier;
}
@(
      title             : '{i18n>SupplierCompany}'
    ) 
entity SupplierCompany : business_child_level_entity {
key supplier : String(10) @(title : '{i18n>SupplierCompany.supplier}');
key companyCode : String(4) @(title : '{i18n>SupplierCompany.companyCode}');
authorizationGroup : String(4) @(title : '{i18n>SupplierCompany.authorizationGroup}');
paymentBlockingReason : String(1) @(title : '{i18n>SupplierCompany.paymentBlockingReason}');
supplierIsBlockedForPosting : Boolean default false @(title : '{i18n>SupplierCompany.supplierIsBlockedForPosting}');
accountingClerk : String(2) @(title : '{i18n>SupplierCompany.accountingClerk}');
accountingClerkFaxNumber : String(31) @(title : '{i18n>SupplierCompany.accountingClerkFaxNumber}');
accountingClerkPhoneNumber : String(30) @(title : '{i18n>SupplierCompany.accountingClerkPhoneNumber}');
supplierClerk : String(15) @(title : '{i18n>SupplierCompany.supplierClerk}');
supplierClerkURL : String(130) @(title : '{i18n>SupplierCompany.supplierClerkURL}');
paymentMethodsList : String(10) @(title : '{i18n>SupplierCompany.paymentMethodsList}');
paymentTerms : String(4) @(title : '{i18n>SupplierCompany.paymentTerms}');
clearCustomerSupplier : Boolean default false @(title : '{i18n>SupplierCompany.clearCustomerSupplier}');
isToBeLocallyProcessed : Boolean default false @(title : '{i18n>SupplierCompany.isToBeLocallyProcessed}');
itemIsToBePaidSeparately : Boolean default false @(title : '{i18n>SupplierCompany.itemIsToBePaidSeparately}');
paymentIsToBeSentByEDI : Boolean default false @(title : '{i18n>SupplierCompany.paymentIsToBeSentByEDI}');
houseBank : String(5) @(title : '{i18n>SupplierCompany.houseBank}');
checkPaidDurationInDays : Decimal(3, 0) @(title : '{i18n>SupplierCompany.checkPaidDurationInDays}');
billOfExchLmtAmtInCoCodeCrcy : Decimal(14, 3) @(title : '{i18n>SupplierCompany.billOfExchLmtAmtInCoCodeCrcy}');
supplierClerkIDBySupplier : String(12) @(title : '{i18n>SupplierCompany.supplierClerkIDBySupplier}');
reconciliationAccount : String(10) @(title : '{i18n>SupplierCompany.reconciliationAccount}');
interestCalculationCode : String(2) @(title : '{i18n>SupplierCompany.interestCalculationCode}');
interestCalculationDate : Date @(title : '{i18n>SupplierCompany.interestCalculationDate}');
intrstCalcFrequencyInMonths : String(2) @(title : '{i18n>SupplierCompany.intrstCalcFrequencyInMonths}');
supplierHeadOffice : String(10) @(title : '{i18n>SupplierCompany.supplierHeadOffice}');
alternativePayee : String(10) @(title : '{i18n>SupplierCompany.alternativePayee}');
layoutSortingRule : String(3) @(title : '{i18n>SupplierCompany.layoutSortingRule}');
aPARToleranceGroup : String(4) @(title : '{i18n>SupplierCompany.aPARToleranceGroup}');
supplierCertificationDate : Date @(title : '{i18n>SupplierCompany.supplierCertificationDate}');
supplierAccountNote : String(30) @(title : '{i18n>SupplierCompany.supplierAccountNote}');
withholdingTaxCountry : String(3) @(title : '{i18n>SupplierCompany.withholdingTaxCountry}');
deletionIndicator : Boolean default false @(title : '{i18n>SupplierCompany.deletionIndicator}');
cashPlanningGroup : String(10) @(title : '{i18n>SupplierCompany.cashPlanningGroup}');
isToBeCheckedForDuplicates : Boolean default false @(title : '{i18n>SupplierCompany.isToBeCheckedForDuplicates}');
keyforDunningNoticeGrouping : String(2) @(title : '{i18n>SupplierCompany.keyforDunningNoticeGrouping}');
previousAccountNumber : String(10) @(title : '{i18n>SupplierCompany.previousAccountNumber}');
personnelNumber : String(8) @(title : '{i18n>SupplierCompany.personnelNumber}');
releaseApprovalGroup : String(4) @(title : '{i18n>SupplierCompany.releaseApprovalGroup}');
minorityIndicator : String(3) @(title : '{i18n>SupplierCompany.minorityIndicator}');
dateOfLastInterestCalRun : Date @(title : '{i18n>SupplierCompany.dateOfLastInterestCalRun}');
withholdingTaxCode : String(2) @(title : '{i18n>SupplierCompany.withholdingTaxCode}');
supplierRecipientType : String(2) @(title : '{i18n>SupplierCompany.supplierRecipientType}');
certNumOfTheWHTaxExemption : String(10) @(title : '{i18n>SupplierCompany.certNumOfTheWHTaxExemption}');
validityDateForWHTaxExemptCert : Date @(title : '{i18n>SupplierCompany.validityDateForWHTaxExemptCert}');
authorityForExemptionFromWHTax : String(1) @(title : '{i18n>SupplierCompany.authorityForExemptionFromWHTax}');
indicatorSendPmntAdviceByXml : String(1) @(title : '{i18n>SupplierCompany.indicatorSendPmntAdviceByXml}');
tolGrpInInvoiceVerification : String(4) @(title : '{i18n>SupplierCompany.tolGrpInInvoiceVerification}');
prepaymentRelevance : String(1) @(title : '{i18n>SupplierCompany.prepaymentRelevance}');
deletionBlock : Boolean default false @(title : '{i18n>SupplierCompany.deletionBlock}');
keyForPaymentGrouping : String(2) @(title : '{i18n>SupplierCompany.keyForPaymentGrouping}');
altPayeeDoc : Boolean default false @(title : '{i18n>SupplierCompany.altPayeeDoc}');

    to_SupplierDunning : Composition of many SupplierDunning
    on to_SupplierDunning.objectID = $self.objectID and to_SupplierDunning.supplier = $self.supplier and to_SupplierDunning.companyCode = $self.companyCode;

    to_SupplierPayee : Composition of many SupplierPayee
    on to_SupplierPayee.objectID = $self.objectID and to_SupplierPayee.supplier = $self.supplier and to_SupplierPayee.companyCode = $self.companyCode;

    to_SupplierWithHoldingTax : Composition of many SupplierWithHoldingTax
    on to_SupplierWithHoldingTax.objectID = $self.objectID and to_SupplierWithHoldingTax.supplier = $self.supplier and to_SupplierWithHoldingTax.companyCode = $self.companyCode;
}
@(
      title             : '{i18n>SupplierPurchasingOrganization}'
    ) 
entity SupplierPurchasingOrganization : business_child_level_entity {
key supplier : String(10) @(title : '{i18n>SupplierPurchasingOrganization.supplier}');
key purchasingOrganization : String(4) @(title : '{i18n>SupplierPurchasingOrganization.purchasingOrganization}');
calculationSchemaGroupCode : String(2) @(title : '{i18n>SupplierPurchasingOrganization.calculationSchemaGroupCode}');
deletionIndicator : Boolean default false @(title : '{i18n>SupplierPurchasingOrganization.deletionIndicator}');
incotermsClassification : String(3) @(title : '{i18n>SupplierPurchasingOrganization.incotermsClassification}');
incotermsTransferLocation : String(28) @(title : '{i18n>SupplierPurchasingOrganization.incotermsTransferLocation}');
incotermsVersion : String(4) @(title : '{i18n>SupplierPurchasingOrganization.incotermsVersion}');
incotermsLocation1 : String(70) @(title : '{i18n>SupplierPurchasingOrganization.incotermsLocation1}');
incotermsLocation2 : String(70) @(title : '{i18n>SupplierPurchasingOrganization.incotermsLocation2}');
invoiceIsGoodsReceiptBased : Boolean default false @(title : '{i18n>SupplierPurchasingOrganization.invoiceIsGoodsReceiptBased}');
materialPlannedDeliveryDurn : Decimal(3, 0) @(title : '{i18n>SupplierPurchasingOrganization.materialPlannedDeliveryDurn}');
minimumOrderAmount : Decimal(14, 3) @(title : '{i18n>SupplierPurchasingOrganization.minimumOrderAmount}');
paymentTerms : String(4) @(title : '{i18n>SupplierPurchasingOrganization.paymentTerms}');
pricingDateControl : String(1) @(title : '{i18n>SupplierPurchasingOrganization.pricingDateControl}');
purOrdAutoGenerationIsAllowed : Boolean default false @(title : '{i18n>SupplierPurchasingOrganization.purOrdAutoGenerationIsAllowed}');
purchaseOrderCurrency : String(5) @(title : '{i18n>SupplierPurchasingOrganization.purchaseOrderCurrency}');
purchasingGroup : String(3) @(title : '{i18n>SupplierPurchasingOrganization.purchasingGroup}');
purchasingIsBlockedForSupplier : Boolean default false @(title : '{i18n>SupplierPurchasingOrganization.purchasingIsBlockedForSupplier}');
shippingCondition : String(2) @(title : '{i18n>SupplierPurchasingOrganization.shippingCondition}');
supplierABCClassificationCode : String(1) @(title : '{i18n>SupplierPurchasingOrganization.supplierABCClassificationCode}');
supplierPhoneNumber : String(16) @(title : '{i18n>SupplierPurchasingOrganization.supplierPhoneNumber}');
supplierRespSalesPersonName : String(30) @(title : '{i18n>SupplierPurchasingOrganization.supplierRespSalesPersonName}');
ourAccountNumWithTheSupplier : String(12) @(title : '{i18n>SupplierPurchasingOrganization.ourAccountNumWithTheSupplier}');
whetherDiscountInKindGranted : String(1) @(title : '{i18n>SupplierPurchasingOrganization.whetherDiscountInKindGranted}');
profileTransMatDataViaIdoc : String(4) @(title : '{i18n>SupplierPurchasingOrganization.profileTransMatDataViaIdoc}');
forSrvBasedInvoiceVerification : Boolean default false @(title : '{i18n>SupplierPurchasingOrganization.forSrvBasedInvoiceVerification}');
supplierSubjectToSubseq : Boolean default false @(title : '{i18n>SupplierPurchasingOrganization.supplierSubjectToSubseq}');
comparisonAgreementOfBizVolume : Boolean default false @(title : '{i18n>SupplierPurchasingOrganization.comparisonAgreementOfBizVolume}');
revaluationAllowed : Boolean default false @(title : '{i18n>SupplierPurchasingOrganization.revaluationAllowed}');
supplierRmaNumberRequired : String(1) @(title : '{i18n>SupplierPurchasingOrganization.supplierRmaNumberRequired}');
relevToPriceDetermination : Boolean default false @(title : '{i18n>SupplierPurchasingOrganization.relevToPriceDetermination}');
indexCompilationForSubseq : Boolean default false @(title : '{i18n>SupplierPurchasingOrganization.indexCompilationForSubseq}');
docIndexCompilationActiveForPO : Boolean default false @(title : '{i18n>SupplierPurchasingOrganization.docIndexCompilationActiveForPO}');
autoEvaluatedRcptForRetItems : Boolean default false @(title : '{i18n>SupplierPurchasingOrganization.autoEvaluatedRcptForRetItems}');
orderAcknowledgmentRequirement : Boolean default false @(title : '{i18n>SupplierPurchasingOrganization.orderAcknowledgmentRequirement}');
modeOfTransportAtTheBorder : String(1) @(title : '{i18n>SupplierPurchasingOrganization.modeOfTransportAtTheBorder}');
unitOfMeasureGroup : String(4) @(title : '{i18n>SupplierPurchasingOrganization.unitOfMeasureGroup}');
roundingProfile : String(4) @(title : '{i18n>SupplierPurchasingOrganization.roundingProfile}');
relevantForSettlementManagemnt : Boolean default false @(title : '{i18n>SupplierPurchasingOrganization.relevantForSettlementManagemnt}');
supSortCriterionForMaterials : String(1) @(title : '{i18n>SupplierPurchasingOrganization.supSortCriterionForMaterials}');
confirmationControlKey : String(4) @(title : '{i18n>SupplierPurchasingOrganization.confirmationControlKey}');
indicateWhetherSupIsReturnsSup : Boolean default false @(title : '{i18n>SupplierPurchasingOrganization.indicateWhetherSupIsReturnsSup}');
evaluatedReceiptSettlement : Boolean default false @(title : '{i18n>SupplierPurchasingOrganization.evaluatedReceiptSettlement}');
planningCalendar : String(3) @(title : '{i18n>SupplierPurchasingOrganization.planningCalendar}');
planningCycle : String(3) @(title : '{i18n>SupplierPurchasingOrganization.planningCycle}');
restrictionPoBasedLoadBuilding : String(4) @(title : '{i18n>SupplierPurchasingOrganization.restrictionPoBasedLoadBuilding}');
officeOfExitEntry4ForeignTrade : String(6) @(title : '{i18n>SupplierPurchasingOrganization.officeOfExitEntry4ForeignTrade}');
orderEntryBySupplier : String(1) @(title : '{i18n>SupplierPurchasingOrganization.orderEntryBySupplier}');
priceMarkingSupplier : String(2) @(title : '{i18n>SupplierPurchasingOrganization.priceMarkingSupplier}');
rackJobbingSupplier : String(1) @(title : '{i18n>SupplierPurchasingOrganization.rackJobbingSupplier}');
supplierServiceLevel : Decimal(4, 1) @(title : '{i18n>SupplierPurchasingOrganization.supplierServiceLevel}');
authorizationGroup : String(4) @(title : '{i18n>SupplierPurchasingOrganization.authorizationGroup}');
supplierAccountGroup : String(4) @(title : '{i18n>SupplierPurchasingOrganization.supplierAccountGroup}');

    to_SupplierPartnerFunction : Composition of many SupplierPartnerFunction
    on to_SupplierPartnerFunction.objectID = $self.objectID and to_SupplierPartnerFunction.supplier = $self.supplier and to_SupplierPartnerFunction.purchasingOrganization = $self.purchasingOrganization;
}
@(
      title             : '{i18n>SupplierVAT}'
    ) 
entity SupplierVAT : business_child_level_entity {
key supplier : String(10) @(title : '{i18n>SupplierVAT.supplier}');
key validDateFrom : Date @(title : '{i18n>SupplierVAT.validDateFrom}');
taxNumber1 : String(16) @(title : '{i18n>SupplierVAT.taxNumber1}');
typeOfBusiness : String(100) @(title : '{i18n>SupplierVAT.typeOfBusiness}');
typeOfIndustry : String(100) @(title : '{i18n>SupplierVAT.typeOfIndustry}');
repName : String(100) @(title : '{i18n>SupplierVAT.repName}');
}
