namespace bp.model.final;
using {
      bp.model.final.AddressUsage,
bp.model.final.AddressVersion,
bp.model.final.AddressEmailAddress,
bp.model.final.AddressFaxNumber,
bp.model.final.AddressMobilePhoneNumber,
bp.model.final.AddressPhoneNumber,
bp.model.final.AddressHomePageUrl,
bp.model.final.CustomerBranchCode,
bp.model.final.CustomerClassification,
bp.model.final.CustomerCompany,
bp.model.final.CustomerSalesArea,
bp.model.final.CustomerVAT,
bp.model.final.SupplierBranchCode,
bp.model.final.SupplierClassification,
bp.model.final.SupplierCompany,
bp.model.final.SupplierPurchasingOrganization,
bp.model.final.SupplierVAT
    } from './3rd-model';
using core.common.business_child_level_entity from '@simplemdg/db_common/db/common-model';
@(
      title             : '{i18n>BusinessPartnerCreditProfile}'
    ) 
entity BusinessPartnerCreditProfile : business_child_level_entity {
key businessPartner : String(10) @(title : '{i18n>BusinessPartnerCreditProfile.businessPartner}');
ownRating : String(10) @(title : '{i18n>BusinessPartnerCreditProfile.ownRating}');
checkRule : String(10) @(title : '{i18n>BusinessPartnerCreditProfile.checkRule}');
limitRule : String(10) @(title : '{i18n>BusinessPartnerCreditProfile.limitRule}');
ratingValueDate : Date @(title : '{i18n>BusinessPartnerCreditProfile.ratingValueDate}');
ratingChangeDate : Date @(title : '{i18n>BusinessPartnerCreditProfile.ratingChangeDate}');
alternateBP : String(10) @(title : '{i18n>BusinessPartnerCreditProfile.alternateBP}');
ownRatingCalc : String(10) @(title : '{i18n>BusinessPartnerCreditProfile.ownRatingCalc}');
riskClass : String(3) @(title : '{i18n>BusinessPartnerCreditProfile.riskClass}');
riskClassCalc : String(3) @(title : '{i18n>BusinessPartnerCreditProfile.riskClassCalc}');
riskClassChangeDate : Date @(title : '{i18n>BusinessPartnerCreditProfile.riskClassChangeDate}');
creditGroup : String(4) @(title : '{i18n>BusinessPartnerCreditProfile.creditGroup}');
}
@(
      title             : '{i18n>BusinessPartnerCreditSegment}'
    ) 
entity BusinessPartnerCreditSegment : business_child_level_entity {
key businessPartner : String(10) @(title : '{i18n>BusinessPartnerCreditSegment.businessPartner}');
key creditSegment : String(10) @(title : '{i18n>BusinessPartnerCreditSegment.creditSegment}');
creditLimit : Decimal(15, 2) @(title : '{i18n>BusinessPartnerCreditSegment.creditLimit}');
isBlocked : Boolean default false @(title : '{i18n>BusinessPartnerCreditSegment.isBlocked}');
limitValidDate : Date @(title : '{i18n>BusinessPartnerCreditSegment.limitValidDate}');
limitChangeDate : Date @(title : '{i18n>BusinessPartnerCreditSegment.limitChangeDate}');
coordinator : String(12) @(title : '{i18n>BusinessPartnerCreditSegment.coordinator}');
custGroup : String(10) @(title : '{i18n>BusinessPartnerCreditSegment.custGroup}');
followUpDate : Date @(title : '{i18n>BusinessPartnerCreditSegment.followUpDate}');
creditLimitCal : Decimal(15, 2) @(title : '{i18n>BusinessPartnerCreditSegment.creditLimitCal}');
isCritical : Boolean default false @(title : '{i18n>BusinessPartnerCreditSegment.isCritical}');
isLimitZero : Boolean default false @(title : '{i18n>BusinessPartnerCreditSegment.isLimitZero}');
isLimitDefined : String(1) @(title : '{i18n>BusinessPartnerCreditSegment.isLimitDefined}');
blockReason : String(2) @(title : '{i18n>BusinessPartnerCreditSegment.blockReason}');
creditLimitReq : Decimal(15, 2) @(title : '{i18n>BusinessPartnerCreditSegment.creditLimitReq}');
isAutomaticReq : Boolean default false @(title : '{i18n>BusinessPartnerCreditSegment.isAutomaticReq}');
validToDate : Date @(title : '{i18n>BusinessPartnerCreditSegment.validToDate}');
reqDate : Date @(title : '{i18n>BusinessPartnerCreditSegment.reqDate}');
isItemError : Boolean default false @(title : '{i18n>BusinessPartnerCreditSegment.isItemError}');
logHNDL : String(22) @(title : '{i18n>BusinessPartnerCreditSegment.logHNDL}');
currency : String(5) @(title : '{i18n>BusinessPartnerCreditSegment.currency}');
}
@(
      title             : '{i18n>BusinessPartnerIdentification}'
    ) 
entity BusinessPartnerIdentification : business_child_level_entity {
key businessPartner : String(10) @(title : '{i18n>BusinessPartnerIdentification.businessPartner}');
key bpIdentificationType : String(6) @(title : '{i18n>BusinessPartnerIdentification.bpIdentificationType}');
key bpIdentificationNumber : String(60) @(title : '{i18n>BusinessPartnerIdentification.bpIdentificationNumber}');
bpIdnNmbrIssuingInstitute : String(40) @(title : '{i18n>BusinessPartnerIdentification.bpIdnNmbrIssuingInstitute}');
bpIdentificationEntryDate : Date @(title : '{i18n>BusinessPartnerIdentification.bpIdentificationEntryDate}');
country : String(3) @(title : '{i18n>BusinessPartnerIdentification.country}');
region : String(3) @(title : '{i18n>BusinessPartnerIdentification.region}');
validityStartDate : Date @(title : '{i18n>BusinessPartnerIdentification.validityStartDate}');
validityEndDate : Date @(title : '{i18n>BusinessPartnerIdentification.validityEndDate}');
authorizationGroup : String(4) @(title : '{i18n>BusinessPartnerIdentification.authorizationGroup}');
}
@(
      title             : '{i18n>BusinessPartnerIndustry}'
    ) 
entity BusinessPartnerIndustry : business_child_level_entity {
key industrySector : String(10) @(title : '{i18n>BusinessPartnerIndustry.industrySector}');
key industrySystemType : String(4) @(title : '{i18n>BusinessPartnerIndustry.industrySystemType}');
key businessPartner : String(10) @(title : '{i18n>BusinessPartnerIndustry.businessPartner}');
isStandardIndustry : String(1) @(title : '{i18n>BusinessPartnerIndustry.isStandardIndustry}');
}
@(
      title             : '{i18n>BusinessPartnerAddress}'
    ) 
entity BusinessPartnerAddress : business_child_level_entity {
key addressID : String(10) @(title : '{i18n>BusinessPartnerAddress.addressID}');
businessPartner : String(10) @(title : '{i18n>BusinessPartnerAddress.businessPartner}');
validityStartDate : DateTime @(title : '{i18n>BusinessPartnerAddress.validityStartDate}');
validityEndDate : DateTime @(title : '{i18n>BusinessPartnerAddress.validityEndDate}');
authorizationGroup : String(4) @(title : '{i18n>BusinessPartnerAddress.authorizationGroup}');
addressUUID : UUID @(title : '{i18n>BusinessPartnerAddress.addressUUID}');
additionalStreetPrefixName : String(40) @(title : '{i18n>BusinessPartnerAddress.additionalStreetPrefixName}');
additionalStreetSuffixName : String(40) @(title : '{i18n>BusinessPartnerAddress.additionalStreetSuffixName}');
addressTimeZone : String(6) @(title : '{i18n>BusinessPartnerAddress.addressTimeZone}');
careOfName : String(40) @(title : '{i18n>BusinessPartnerAddress.careOfName}');
cityCode : String(12) @(title : '{i18n>BusinessPartnerAddress.cityCode}');
cityName : String(40) @(title : '{i18n>BusinessPartnerAddress.cityName}');
companyPostalCode : String(10) @(title : '{i18n>BusinessPartnerAddress.companyPostalCode}');
country : String(3) @(title : '{i18n>BusinessPartnerAddress.country}');
county : String(40) @(title : '{i18n>BusinessPartnerAddress.county}');
deliveryServiceNumber : String(10) @(title : '{i18n>BusinessPartnerAddress.deliveryServiceNumber}');
deliveryServiceTypeCode : String(4) @(title : '{i18n>BusinessPartnerAddress.deliveryServiceTypeCode}');
district : String(40) @(title : '{i18n>BusinessPartnerAddress.district}');
formOfAddress : String(4) @(title : '{i18n>BusinessPartnerAddress.formOfAddress}');
fullName : String(80) @(title : '{i18n>BusinessPartnerAddress.fullName}');
homeCityName : String(40) @(title : '{i18n>BusinessPartnerAddress.homeCityName}');
houseNumber : String(10) @(title : '{i18n>BusinessPartnerAddress.houseNumber}');
houseNumberSupplementText : String(10) @(title : '{i18n>BusinessPartnerAddress.houseNumberSupplementText}');
language : String(2) @(title : '{i18n>BusinessPartnerAddress.language}');
poBox : String(10) @(title : '{i18n>BusinessPartnerAddress.poBox}');
poBoxDeviatingCityName : String(40) @(title : '{i18n>BusinessPartnerAddress.poBoxDeviatingCityName}');
poBoxDeviatingCountry : String(3) @(title : '{i18n>BusinessPartnerAddress.poBoxDeviatingCountry}');
poBoxDeviatingRegion : String(3) @(title : '{i18n>BusinessPartnerAddress.poBoxDeviatingRegion}');
poBoxIsWithoutNumber : Boolean default false @(title : '{i18n>BusinessPartnerAddress.poBoxIsWithoutNumber}');
poBoxLobbyName : String(40) @(title : '{i18n>BusinessPartnerAddress.poBoxLobbyName}');
poBoxPostalCode : String(10) @(title : '{i18n>BusinessPartnerAddress.poBoxPostalCode}');
person : String(10) @(title : '{i18n>BusinessPartnerAddress.person}');
postalCode : String(10) @(title : '{i18n>BusinessPartnerAddress.postalCode}');
prfrdCommMediumType : String(3) @(title : '{i18n>BusinessPartnerAddress.prfrdCommMediumType}');
region : String(3) @(title : '{i18n>BusinessPartnerAddress.region}');
streetName : String(60) @(title : '{i18n>BusinessPartnerAddress.streetName}');
streetPrefixName : String(40) @(title : '{i18n>BusinessPartnerAddress.streetPrefixName}');
streetSuffixName : String(40) @(title : '{i18n>BusinessPartnerAddress.streetSuffixName}');
taxJurisdiction : String(15) @(title : '{i18n>BusinessPartnerAddress.taxJurisdiction}');
transportZone : String(10) @(title : '{i18n>BusinessPartnerAddress.transportZone}');
building : String(20) @(title : '{i18n>BusinessPartnerAddress.building}');
floor : String(10) @(title : '{i18n>BusinessPartnerAddress.floor}');
roomNumber : String(10) @(title : '{i18n>BusinessPartnerAddress.roomNumber}');
regionalStructureGrouping : String(8) @(title : '{i18n>BusinessPartnerAddress.regionalStructureGrouping}');
streetAddressUndeliverableFlag : String(4) @(title : '{i18n>BusinessPartnerAddress.streetAddressUndeliverableFlag}');
poBoxAddressUndeliverableFlag : String(4) @(title : '{i18n>BusinessPartnerAddress.poBoxAddressUndeliverableFlag}');
addressIDByExternalSystem : String(20) @(title : '{i18n>BusinessPartnerAddress.addressIDByExternalSystem}');

    to_AddressUsage : Composition of many AddressUsage
    on to_AddressUsage.objectID = $self.objectID and to_AddressUsage.addressID = $self.addressID and to_AddressUsage.businessPartner = $self.businessPartner;

    to_AddressVersion : Composition of many AddressVersion
    on to_AddressVersion.objectID = $self.objectID and to_AddressVersion.addressID = $self.addressID;

    to_EmailAddress : Composition of many AddressEmailAddress
    on to_EmailAddress.objectID = $self.objectID and to_EmailAddress.addressID = $self.addressID;

    to_FaxNumber : Composition of many AddressFaxNumber
    on to_FaxNumber.objectID = $self.objectID and to_FaxNumber.addressID = $self.addressID;

    to_MobilePhoneNumber : Composition of many AddressMobilePhoneNumber
    on to_MobilePhoneNumber.objectID = $self.objectID and to_MobilePhoneNumber.addressID = $self.addressID;

    to_PhoneNumber : Composition of many AddressPhoneNumber
    on to_PhoneNumber.objectID = $self.objectID and to_PhoneNumber.addressID = $self.addressID;

    to_URLAddress : Composition of many AddressHomePageUrl
    on to_URLAddress.objectID = $self.objectID and to_URLAddress.addressID = $self.addressID;
}
@(
      title             : '{i18n>BusinessPartnerBank}'
    ) 
entity BusinessPartnerBank : business_child_level_entity {
key businessPartner : String(10) @(title : '{i18n>BusinessPartnerBank.businessPartner}');
key bankIdentification : String(4) @(title : '{i18n>BusinessPartnerBank.bankIdentification}');
bankCountryKey : String(3) @(title : '{i18n>BusinessPartnerBank.bankCountryKey}');
bankName : String(60) @(title : '{i18n>BusinessPartnerBank.bankName}');
bankNumber : String(15) @(title : '{i18n>BusinessPartnerBank.bankNumber}');
swiftCode : String(11) @(title : '{i18n>BusinessPartnerBank.swiftCode}');
bankControlKey : String(2) @(title : '{i18n>BusinessPartnerBank.bankControlKey}');
bankAccountHolderName : String(60) @(title : '{i18n>BusinessPartnerBank.bankAccountHolderName}');
bankAccountName : String(40) @(title : '{i18n>BusinessPartnerBank.bankAccountName}');
validityStartDate : DateTime @(title : '{i18n>BusinessPartnerBank.validityStartDate}');
validityEndDate : DateTime @(title : '{i18n>BusinessPartnerBank.validityEndDate}');
iBAN : String(34) @(title : '{i18n>BusinessPartnerBank.iBAN}');
iBANValidityStartDate : Date @(title : '{i18n>BusinessPartnerBank.iBANValidityStartDate}');
bankAccount : String(18) @(title : '{i18n>BusinessPartnerBank.bankAccount}');
bankAccountReferenceText : String(20) @(title : '{i18n>BusinessPartnerBank.bankAccountReferenceText}');
collectionAuthInd : Boolean default false @(title : '{i18n>BusinessPartnerBank.collectionAuthInd}');
cityName : String(35) @(title : '{i18n>BusinessPartnerBank.cityName}');
}
@(
      title             : '{i18n>BusinessPartnerCard}'
    ) 
entity BusinessPartnerCard : business_child_level_entity {
key businessPartner : String(10) @(title : '{i18n>BusinessPartnerCard.businessPartner}');
key paymentCardID : String(6) @(title : '{i18n>BusinessPartnerCard.paymentCardID}');
cardType : String(4) @(title : '{i18n>BusinessPartnerCard.cardType}');
cardNumber : String(25) @(title : '{i18n>BusinessPartnerCard.cardNumber}');
standardCard : Boolean default false @(title : '{i18n>BusinessPartnerCard.standardCard}');
nameOfCardHolder : String(40) @(title : '{i18n>BusinessPartnerCard.nameOfCardHolder}');
cardDetail : String(40) @(title : '{i18n>BusinessPartnerCard.cardDetail}');
cardGuid : UUID @(title : '{i18n>BusinessPartnerCard.cardGuid}');
validFrom : Date @(title : '{i18n>BusinessPartnerCard.validFrom}');
validTo : Date @(title : '{i18n>BusinessPartnerCard.validTo}');
issuingBank : String(40) @(title : '{i18n>BusinessPartnerCard.issuingBank}');
dateOfIssue : Date @(title : '{i18n>BusinessPartnerCard.dateOfIssue}');
maskNumber : String(25) @(title : '{i18n>BusinessPartnerCard.maskNumber}');
blockingReason : String(2) @(title : '{i18n>BusinessPartnerCard.blockingReason}');
}
@(
      title             : '{i18n>BusinessPartnerRelationship}'
    ) 
entity BusinessPartnerRelationship : business_child_level_entity {
key businessPartner : String(10) @(title : '{i18n>BusinessPartnerRelationship.businessPartner}');
key relationshipBusinessPartner : String(10) @(title : '{i18n>BusinessPartnerRelationship.relationshipBusinessPartner}');
key relationshipNumber : String(12) @(title : '{i18n>BusinessPartnerRelationship.relationshipNumber}');
relationshipCategory : String(7) @(title : '{i18n>BusinessPartnerRelationship.relationshipCategory}');
relationshipType : String(4) @(title : '{i18n>BusinessPartnerRelationship.relationshipType}');
validFromDate : Date @(title : '{i18n>BusinessPartnerRelationship.validFromDate}');
validUntilDate : Date @(title : '{i18n>BusinessPartnerRelationship.validUntilDate}');
differentiationTypeValue : String(20) @(title : '{i18n>BusinessPartnerRelationship.differentiationTypeValue}');
}
@(
      title             : '{i18n>BusinessPartnerRole}'
    ) 
entity BusinessPartnerRole : business_child_level_entity {
key businessPartner : String(10) @(title : '{i18n>BusinessPartnerRole.businessPartner}');
key businessPartnerRole : String(6) @(title : '{i18n>BusinessPartnerRole.businessPartnerRole}');
validFrom : DateTime @(title : '{i18n>BusinessPartnerRole.validFrom}');
validTo : DateTime @(title : '{i18n>BusinessPartnerRole.validTo}');
authorizationGroup : String(4) @(title : '{i18n>BusinessPartnerRole.authorizationGroup}');
}
@(
      title             : '{i18n>BusinessPartnerTax}'
    ) 
entity BusinessPartnerTax : business_child_level_entity {
key businessPartner : String(10) @(title : '{i18n>BusinessPartnerTax.businessPartner}');
key bpTaxTypeItem : String(4) @(title : '{i18n>BusinessPartnerTax.bpTaxTypeItem}');
bpTaxType : String(4) @(title : '{i18n>BusinessPartnerTax.bpTaxType}');
bpTaxNumber : String(20) @(title : '{i18n>BusinessPartnerTax.bpTaxNumber}');
bpTaxLongNumber : String(60) @(title : '{i18n>BusinessPartnerTax.bpTaxLongNumber}');
authorizationGroup : String(4) @(title : '{i18n>BusinessPartnerTax.authorizationGroup}');
}
@(
      title             : '{i18n>Customer}'
    ) 
entity Customer : business_child_level_entity {
key customer : String(10) @(title : '{i18n>Customer.customer}');
authorizationGroup : String(4) @(title : '{i18n>Customer.authorizationGroup}');
billingIsBlockedForCustomer : String(2) @(title : '{i18n>Customer.billingIsBlockedForCustomer}');
createdByUser : String(12) @(title : '{i18n>Customer.createdByUser}');
creationDate : Date @(title : '{i18n>Customer.creationDate}');
customerAccountGroup : String(4) @(title : '{i18n>Customer.customerAccountGroup}');
customerClassification : String(2) @(title : '{i18n>Customer.customerClassification}');
customerFullName : String(220) @(title : '{i18n>Customer.customerFullName}');
customerName : String(80) @(title : '{i18n>Customer.customerName}');
deliveryIsBlocked : String(2) @(title : '{i18n>Customer.deliveryIsBlocked}');
nFPartnerIsNaturalPerson : String(1) @(title : '{i18n>Customer.nFPartnerIsNaturalPerson}');
orderIsBlockedForCustomer : String(2) @(title : '{i18n>Customer.orderIsBlockedForCustomer}');
postingIsBlocked : Boolean default false @(title : '{i18n>Customer.postingIsBlocked}');
supplier : String(10) @(title : '{i18n>Customer.supplier}');
customerCorporateGroup : String(10) @(title : '{i18n>Customer.customerCorporateGroup}');
fiscalAddress : String(10) @(title : '{i18n>Customer.fiscalAddress}');
industry : String(4) @(title : '{i18n>Customer.industry}');
industryCode1 : String(10) @(title : '{i18n>Customer.industryCode1}');
industryCode2 : String(10) @(title : '{i18n>Customer.industryCode2}');
industryCode3 : String(10) @(title : '{i18n>Customer.industryCode3}');
industryCode4 : String(10) @(title : '{i18n>Customer.industryCode4}');
industryCode5 : String(10) @(title : '{i18n>Customer.industryCode5}');
internationalLocationNumber1 : String(7) @(title : '{i18n>Customer.internationalLocationNumber1}');
nielsenRegion : String(2) @(title : '{i18n>Customer.nielsenRegion}');
responsibleType : String(2) @(title : '{i18n>Customer.responsibleType}');
taxNumber1 : String(16) @(title : '{i18n>Customer.taxNumber1}');
taxNumber2 : String(11) @(title : '{i18n>Customer.taxNumber2}');
taxNumber3 : String(18) @(title : '{i18n>Customer.taxNumber3}');
taxNumber4 : String(18) @(title : '{i18n>Customer.taxNumber4}');
taxNumber5 : String(60) @(title : '{i18n>Customer.taxNumber5}');
taxNumberType : String(2) @(title : '{i18n>Customer.taxNumberType}');
vATRegistration : String(20) @(title : '{i18n>Customer.vATRegistration}');
deletionIndicator : Boolean default false @(title : '{i18n>Customer.deletionIndicator}');
alternativePayerAccount : String(10) @(title : '{i18n>Customer.alternativePayerAccount}');
vATLiability : Boolean default false @(title : '{i18n>Customer.vATLiability}');
bR_SUFRAMACode : String(9) @(title : '{i18n>Customer.bR_SUFRAMACode}');
regionalMarket : String(5) @(title : '{i18n>Customer.regionalMarket}');
expressTrainStation : String(25) @(title : '{i18n>Customer.expressTrainStation}');
trainStation : String(25) @(title : '{i18n>Customer.trainStation}');
cityCoordinates : String(10) @(title : '{i18n>Customer.cityCoordinates}');
annualSales : Decimal(16, 3) @(title : '{i18n>Customer.annualSales}');
currencyOfSalesFigure : String(5) @(title : '{i18n>Customer.currencyOfSalesFigure}');
yearForWhichSalesAreGiven : String(4) @(title : '{i18n>Customer.yearForWhichSalesAreGiven}');
plant : String(4) @(title : '{i18n>Customer.plant}');
isAnAltPayerAllowedInDoc : Boolean default false @(title : '{i18n>Customer.isAnAltPayerAllowedInDoc}');
fiscalYearVariant : String(2) @(title : '{i18n>Customer.fiscalYearVariant}');
indicatorCompetitor : Boolean default false @(title : '{i18n>Customer.indicatorCompetitor}');
indicatorSalesPartner : Boolean default false @(title : '{i18n>Customer.indicatorSalesPartner}');
indicatorSalesProspect : String(1) @(title : '{i18n>Customer.indicatorSalesProspect}');
iDForDefaultSoldToParty : Boolean default false @(title : '{i18n>Customer.iDForDefaultSoldToParty}');
indicatorConsumer : Boolean default false @(title : '{i18n>Customer.indicatorConsumer}');
deletionBlockForMasterRecord : Boolean default false @(title : '{i18n>Customer.deletionBlockForMasterRecord}');
bPSubjectToEqualizationTax : String(1) @(title : '{i18n>Customer.bPSubjectToEqualizationTax}');
countyCode : String(3) @(title : '{i18n>Customer.countyCode}');
cityCode : String(4) @(title : '{i18n>Customer.cityCode}');
rGNumber : String(11) @(title : '{i18n>Customer.rGNumber}');
issuedBy : String(3) @(title : '{i18n>Customer.issuedBy}');
state : String(2) @(title : '{i18n>Customer.state}');
rGIssuingDate : Date @(title : '{i18n>Customer.rGIssuingDate}');
rICNumbeEF42 : String(11) @(title : '{i18n>Customer.rICNumbeEF42}');
foreignNationalRegistration : String(10) @(title : '{i18n>Customer.foreignNationalRegistration}');
rNEIssuingDate : Date @(title : '{i18n>Customer.rNEIssuingDate}');
cNAE : String(7) @(title : '{i18n>Customer.cNAE}');
legalNature : String(4) @(title : '{i18n>Customer.legalNature}');
cRTNumber : String(1) @(title : '{i18n>Customer.cRTNumber}');
iCMSTaxpayer : String(2) @(title : '{i18n>Customer.iCMSTaxpayer}');
industryMainType : String(2) @(title : '{i18n>Customer.industryMainType}');
taxDeclarationType : String(2) @(title : '{i18n>Customer.taxDeclarationType}');
companySize : String(2) @(title : '{i18n>Customer.companySize}');
declarationRegimen : String(2) @(title : '{i18n>Customer.declarationRegimen}');
attribute1 : String(2) @(title : '{i18n>Customer.attribute1}');
attribute2 : String(2) @(title : '{i18n>Customer.attribute2}');
attribute3 : String(2) @(title : '{i18n>Customer.attribute3}');
attribute4 : String(2) @(title : '{i18n>Customer.attribute4}');
attribute5 : String(2) @(title : '{i18n>Customer.attribute5}');
attribute6 : String(3) @(title : '{i18n>Customer.attribute6}');
attribute7 : String(3) @(title : '{i18n>Customer.attribute7}');
attribute8 : String(3) @(title : '{i18n>Customer.attribute8}');
attribute9 : String(3) @(title : '{i18n>Customer.attribute9}');
attribute10 : String(3) @(title : '{i18n>Customer.attribute10}');
customerConditionGroup1 : String(2) @(title : '{i18n>Customer.customerConditionGroup1}');
customerConditionGroup2 : String(2) @(title : '{i18n>Customer.customerConditionGroup2}');
customerConditionGroup3 : String(2) @(title : '{i18n>Customer.customerConditionGroup3}');
customerConditionGroup4 : String(2) @(title : '{i18n>Customer.customerConditionGroup4}');
customerConditionGroup5 : String(2) @(title : '{i18n>Customer.customerConditionGroup5}');
blockSalesSupport : String(2) @(title : '{i18n>Customer.blockSalesSupport}');

    to_CustomerBranchCode : Composition of many CustomerBranchCode
    on to_CustomerBranchCode.objectID = $self.objectID and to_CustomerBranchCode.customer = $self.customer;

    to_CustomerClassification : Composition of many CustomerClassification
    on to_CustomerClassification.objectID = $self.objectID and to_CustomerClassification.customer = $self.customer;

    to_CustomerCompany : Composition of many CustomerCompany
    on to_CustomerCompany.objectID = $self.objectID and to_CustomerCompany.customer = $self.customer;

    to_CustomerSalesArea : Composition of many CustomerSalesArea
    on to_CustomerSalesArea.objectID = $self.objectID and to_CustomerSalesArea.customer = $self.customer;

    to_CustomerVAT : Composition of many CustomerVAT
    on to_CustomerVAT.objectID = $self.objectID and to_CustomerVAT.customer = $self.customer;
}
@(
      title             : '{i18n>Supplier}'
    ) 
entity Supplier : business_child_level_entity {
key supplier : String(10) @(title : '{i18n>Supplier.supplier}');
alternativePayeeAccountNumber : String(10) @(title : '{i18n>Supplier.alternativePayeeAccountNumber}');
authorizationGroup : String(4) @(title : '{i18n>Supplier.authorizationGroup}');
createdByUser : String(12) @(title : '{i18n>Supplier.createdByUser}');
creationDate : Date @(title : '{i18n>Supplier.creationDate}');
customer : String(10) @(title : '{i18n>Supplier.customer}');
paymentIsBlockedForSupplier : Boolean default false @(title : '{i18n>Supplier.paymentIsBlockedForSupplier}');
postingIsBlocked : Boolean default false @(title : '{i18n>Supplier.postingIsBlocked}');
purchasingIsBlocked : Boolean default false @(title : '{i18n>Supplier.purchasingIsBlocked}');
supplierAccountGroup : String(4) @(title : '{i18n>Supplier.supplierAccountGroup}');
supplierFullName : String(220) @(title : '{i18n>Supplier.supplierFullName}');
supplierName : String(80) @(title : '{i18n>Supplier.supplierName}');
vATRegistration : String(20) @(title : '{i18n>Supplier.vATRegistration}');
birthDate : Date @(title : '{i18n>Supplier.birthDate}');
concatenatedInternationalLocNo : String(20) @(title : '{i18n>Supplier.concatenatedInternationalLocNo}');
deletionIndicator : Boolean default false @(title : '{i18n>Supplier.deletionIndicator}');
fiscalAddress : String(10) @(title : '{i18n>Supplier.fiscalAddress}');
industry : String(4) @(title : '{i18n>Supplier.industry}');
internationalLocationNumber1 : String(7) @(title : '{i18n>Supplier.internationalLocationNumber1}');
internationalLocationNumber2 : String(5) @(title : '{i18n>Supplier.internationalLocationNumber2}');
internationalLocationNumber3 : String(1) @(title : '{i18n>Supplier.internationalLocationNumber3}');
isNaturalPerson : String(1) @(title : '{i18n>Supplier.isNaturalPerson}');
responsibleType : String(2) @(title : '{i18n>Supplier.responsibleType}');
suplrQltyInProcmtCertfnValidTo : Date @(title : '{i18n>Supplier.suplrQltyInProcmtCertfnValidTo}');
suplrQualityManagementSystem : String(4) @(title : '{i18n>Supplier.suplrQualityManagementSystem}');
supplierCorporateGroup : String(10) @(title : '{i18n>Supplier.supplierCorporateGroup}');
supplierProcurementBlock : String(2) @(title : '{i18n>Supplier.supplierProcurementBlock}');
taxNumber1 : String(16) @(title : '{i18n>Supplier.taxNumber1}');
taxNumber2 : String(11) @(title : '{i18n>Supplier.taxNumber2}');
taxNumber3 : String(18) @(title : '{i18n>Supplier.taxNumber3}');
taxNumber4 : String(18) @(title : '{i18n>Supplier.taxNumber4}');
taxNumber5 : String(60) @(title : '{i18n>Supplier.taxNumber5}');
taxNumberResponsible : String(18) @(title : '{i18n>Supplier.taxNumberResponsible}');
taxNumberType : String(2) @(title : '{i18n>Supplier.taxNumberType}');
referenceAccountGroup : String(4) @(title : '{i18n>Supplier.referenceAccountGroup}');
businessType : String(30) @(title : '{i18n>Supplier.businessType}');
industryType : String(30) @(title : '{i18n>Supplier.industryType}');
taxInvoiceRepresentativeName : String(10) @(title : '{i18n>Supplier.taxInvoiceRepresentativeName}');
vATLiability : Boolean default false @(title : '{i18n>Supplier.vATLiability}');
lastExternalReview : Date @(title : '{i18n>Supplier.lastExternalReview}');
accNoOfMasterRecOfTaxOfficeRes : String(10) @(title : '{i18n>Supplier.accNoOfMasterRecOfTaxOfficeRes}');
termsOfLiability : Boolean default false @(title : '{i18n>Supplier.termsOfLiability}');
microCompanyIndicator : Boolean default false @(title : '{i18n>Supplier.microCompanyIndicator}');
companySize : String(2) @(title : '{i18n>Supplier.companySize}');
declarationRegimenForPisCofins : String(2) @(title : '{i18n>Supplier.declarationRegimenForPisCofins}');
crcNumber : String(25) @(title : '{i18n>Supplier.crcNumber}');
rgNumber : String(11) @(title : '{i18n>Supplier.rgNumber}');
issuedBy : String(3) @(title : '{i18n>Supplier.issuedBy}');
state : String(2) @(title : '{i18n>Supplier.state}');
rgIssuingDate : Date @(title : '{i18n>Supplier.rgIssuingDate}');
ricNumber : String(11) @(title : '{i18n>Supplier.ricNumber}');
foreignNationalRegistration : String(10) @(title : '{i18n>Supplier.foreignNationalRegistration}');
rneIssuingDate : Date @(title : '{i18n>Supplier.rneIssuingDate}');
cnae : String(7) @(title : '{i18n>Supplier.cnae}');
legalNature : String(4) @(title : '{i18n>Supplier.legalNature}');
crtNumber : String(1) @(title : '{i18n>Supplier.crtNumber}');
icmsTaxpayer : String(2) @(title : '{i18n>Supplier.icmsTaxpayer}');
industryMainType : String(2) @(title : '{i18n>Supplier.industryMainType}');
taxDeclarationType : String(2) @(title : '{i18n>Supplier.taxDeclarationType}');
supplierSubRangeRelevantInd : Boolean default false @(title : '{i18n>Supplier.supplierSubRangeRelevantInd}');
indicatorPlantLevelRelevant : Boolean default false @(title : '{i18n>Supplier.indicatorPlantLevelRelevant}');
factoryCalendarKey : String(2) @(title : '{i18n>Supplier.factoryCalendarKey}');
annualRepetitionOfDateLimit : Boolean default false @(title : '{i18n>Supplier.annualRepetitionOfDateLimit}');
dateLimitForExternalDocID : Date @(title : '{i18n>Supplier.dateLimitForExternalDocID}');
trainStation : String(25) @(title : '{i18n>Supplier.trainStation}');
reportKeyForDataMediumExchange : String(1) @(title : '{i18n>Supplier.reportKeyForDataMediumExchange}');
instructionKeyForDataMedEx : String(2) @(title : '{i18n>Supplier.instructionKeyForDataMedEx}');
alternativePayeeInDocAllowed : Boolean default false @(title : '{i18n>Supplier.alternativePayeeInDocAllowed}');
isrSubscriberNumber : String(11) @(title : '{i18n>Supplier.isrSubscriberNumber}');
registeredForSocialInsurance : Boolean default false @(title : '{i18n>Supplier.registeredForSocialInsurance}');
activityCodeForSocialInsurance : String(3) @(title : '{i18n>Supplier.activityCodeForSocialInsurance}');
standardCarrierAccessCode : String(4) @(title : '{i18n>Supplier.standardCarrierAccessCode}');
serviceAgentProcedureGroup : String(4) @(title : '{i18n>Supplier.serviceAgentProcedureGroup}');
forwardingAgentFreightGroup : String(4) @(title : '{i18n>Supplier.forwardingAgentFreightGroup}');
profession : String(30) @(title : '{i18n>Supplier.profession}');
shipmentStatsGrpTransSrvAgent : String(2) @(title : '{i18n>Supplier.shipmentStatsGrpTransSrvAgent}');
extManufacturerCodeNameOrNum : String(10) @(title : '{i18n>Supplier.extManufacturerCodeNameOrNum}');
supIndRelevantForProofOfDlv : String(1) @(title : '{i18n>Supplier.supIndRelevantForProofOfDlv}');
carrierConfirmationIsExpected : String(1) @(title : '{i18n>Supplier.carrierConfirmationIsExpected}');
centralDelBlockForMasterRecord : Boolean default false @(title : '{i18n>Supplier.centralDelBlockForMasterRecord}');
alternativePayeeUsingAccNum : Boolean default false @(title : '{i18n>Supplier.alternativePayeeUsingAccNum}');
taxBaseInPercentage : String(1) @(title : '{i18n>Supplier.taxBaseInPercentage}');
taxSplit : Boolean default false @(title : '{i18n>Supplier.taxSplit}');
bPSubjectToEqualizationTax : String(1) @(title : '{i18n>Supplier.bPSubjectToEqualizationTax}');
creditInformationNumber : String(11) @(title : '{i18n>Supplier.creditInformationNumber}');
plant : String(4) @(title : '{i18n>Supplier.plant}');

    to_SupplierBranchCode : Composition of many SupplierBranchCode
    on to_SupplierBranchCode.objectID = $self.objectID and to_SupplierBranchCode.supplier = $self.supplier;

    to_SupplierClassification : Composition of many SupplierClassification
    on to_SupplierClassification.objectID = $self.objectID and to_SupplierClassification.supplier = $self.supplier;

    to_SupplierCompany : Composition of many SupplierCompany
    on to_SupplierCompany.objectID = $self.objectID and to_SupplierCompany.supplier = $self.supplier;

    to_SupplierPurchasingOrg : Composition of many SupplierPurchasingOrganization
    on to_SupplierPurchasingOrg.objectID = $self.objectID and to_SupplierPurchasingOrg.supplier = $self.supplier;

    to_SupplierVAT : Composition of many SupplierVAT
    on to_SupplierVAT.objectID = $self.objectID and to_SupplierVAT.supplier = $self.supplier;
}
