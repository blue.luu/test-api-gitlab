namespace bp.model.final;
using {
      bp.model.final.BusinessPartnerCreditProfile,
bp.model.final.BusinessPartnerCreditSegment,
bp.model.final.BusinessPartnerIdentification,
bp.model.final.BusinessPartnerIndustry,
bp.model.final.BusinessPartnerAddress,
bp.model.final.BusinessPartnerBank,
bp.model.final.BusinessPartnerCard,
bp.model.final.BusinessPartnerRelationship,
bp.model.final.BusinessPartnerRole,
bp.model.final.BusinessPartnerTax,
bp.model.final.Customer,
bp.model.final.Supplier
    } from './2nd-model';
using core.common.business_1st_level_entity from '@simplemdg/db_common/db/common-model';
@(
      title             : '{i18n>BusinessPartner}'
    ) 
entity BusinessPartner : business_1st_level_entity {
key businessPartner : String(10) @(title : '{i18n>BusinessPartner.businessPartner}');
customer : String(10) @(title : '{i18n>BusinessPartner.customer}');
supplier : String(10) @(title : '{i18n>BusinessPartner.supplier}');
academicTitle : String(4) @(title : '{i18n>BusinessPartner.academicTitle}');
authorizationGroup : String(4) @(title : '{i18n>BusinessPartner.authorizationGroup}');
businessPartnerCategory : String(1) @(title : '{i18n>BusinessPartner.businessPartnerCategory}');
businessPartnerFullName : String(81) @(title : '{i18n>BusinessPartner.businessPartnerFullName}');
businessPartnerGrouping : String(4) @(title : '{i18n>BusinessPartner.businessPartnerGrouping}');
businessPartnerName : String(81) @(title : '{i18n>BusinessPartner.businessPartnerName}');
businessPartnerUUID : UUID @(title : '{i18n>BusinessPartner.businessPartnerUUID}');
correspondenceLanguage : String(2) @(title : '{i18n>BusinessPartner.correspondenceLanguage}');
createdByUser : String(12) @(title : '{i18n>BusinessPartner.createdByUser}');
creationDate : Date @(title : '{i18n>BusinessPartner.creationDate}');
creationTime : Time @(title : '{i18n>BusinessPartner.creationTime}');
firstName : String(40) @(title : '{i18n>BusinessPartner.firstName}');
formOfAddress : String(4) @(title : '{i18n>BusinessPartner.formOfAddress}');
industry : String(10) @(title : '{i18n>BusinessPartner.industry}');
internationalLocationNumber1 : String(7) @(title : '{i18n>BusinessPartner.internationalLocationNumber1}');
internationalLocationNumber2 : String(5) @(title : '{i18n>BusinessPartner.internationalLocationNumber2}');
isFemale : Boolean default false @(title : '{i18n>BusinessPartner.isFemale}');
isMale : Boolean default false @(title : '{i18n>BusinessPartner.isMale}');
isNaturalPerson : String(1) @(title : '{i18n>BusinessPartner.isNaturalPerson}');
isSexUnknown : Boolean default false @(title : '{i18n>BusinessPartner.isSexUnknown}');
language : String(2) @(title : '{i18n>BusinessPartner.language}');
lastChangeDate : Date @(title : '{i18n>BusinessPartner.lastChangeDate}');
lastChangeTime : Time @(title : '{i18n>BusinessPartner.lastChangeTime}');
lastChangedByUser : String(12) @(title : '{i18n>BusinessPartner.lastChangedByUser}');
lastName : String(40) @(title : '{i18n>BusinessPartner.lastName}');
legalForm : String(2) @(title : '{i18n>BusinessPartner.legalForm}');
organizationBPName1 : String(40) @(title : '{i18n>BusinessPartner.organizationBPName1}');
organizationBPName2 : String(40) @(title : '{i18n>BusinessPartner.organizationBPName2}');
organizationBPName3 : String(40) @(title : '{i18n>BusinessPartner.organizationBPName3}');
organizationBPName4 : String(40) @(title : '{i18n>BusinessPartner.organizationBPName4}');
organizationFoundationDate : Date @(title : '{i18n>BusinessPartner.organizationFoundationDate}');
organizationLiquidationDate : Date @(title : '{i18n>BusinessPartner.organizationLiquidationDate}');
searchTerm1 : String(20) @(title : '{i18n>BusinessPartner.searchTerm1}');
searchTerm2 : String(20) @(title : '{i18n>BusinessPartner.searchTerm2}');
additionalLastName : String(40) @(title : '{i18n>BusinessPartner.additionalLastName}');
nameAtBirth : String(40) @(title : '{i18n>BusinessPartner.nameAtBirth}');
birthDate : Date @(title : '{i18n>BusinessPartner.birthDate}');
businessPartnerIsBlocked : Boolean default false @(title : '{i18n>BusinessPartner.businessPartnerIsBlocked}');
businessPartnerType : String(4) @(title : '{i18n>BusinessPartner.businessPartnerType}');
eTag : String(26) @(title : '{i18n>BusinessPartner.eTag}');
groupBusinessPartnerName1 : String(40) @(title : '{i18n>BusinessPartner.groupBusinessPartnerName1}');
groupBusinessPartnerName2 : String(40) @(title : '{i18n>BusinessPartner.groupBusinessPartnerName2}');
independentAddressID : String(10) @(title : '{i18n>BusinessPartner.independentAddressID}');
internationalLocationNumber3 : String(1) @(title : '{i18n>BusinessPartner.internationalLocationNumber3}');
middleName : String(40) @(title : '{i18n>BusinessPartner.middleName}');
nameCountry : String(3) @(title : '{i18n>BusinessPartner.nameCountry}');
nameFormat : String(2) @(title : '{i18n>BusinessPartner.nameFormat}');
personFullName : String(80) @(title : '{i18n>BusinessPartner.personFullName}');
personNumber : String(10) @(title : '{i18n>BusinessPartner.personNumber}');
isMarkedForArchiving : Boolean default false @(title : '{i18n>BusinessPartner.isMarkedForArchiving}');
businessPartnerIDByExtSystem : String(20) @(title : '{i18n>BusinessPartner.businessPartnerIDByExtSystem}');
deathDate : Date @(title : '{i18n>BusinessPartner.deathDate}');
employer : String(35) @(title : '{i18n>BusinessPartner.employer}');
countryOfOrigin : String(3) @(title : '{i18n>BusinessPartner.countryOfOrigin}');
legalEntityOfOrganization : String(2) @(title : '{i18n>BusinessPartner.legalEntityOfOrganization}');
trdCmplncLicenseIsMilitarySctr : Boolean default false @(title : '{i18n>BusinessPartner.trdCmplncLicenseIsMilitarySctr}');
trdCmplncLicenseIsNuclearSctr : String(1) @(title : '{i18n>BusinessPartner.trdCmplncLicenseIsNuclearSctr}');
birthPlace : String(40) @(title : '{i18n>BusinessPartner.birthPlace}');
contactPermission : String(1) @(title : '{i18n>BusinessPartner.contactPermission}');
tradingPartner : String(6) @(title : '{i18n>BusinessPartner.tradingPartner}');
businessPartnerIsNotReleased : Boolean default false @(title : '{i18n>BusinessPartner.businessPartnerIsNotReleased}');
businessPartnerOccupation : String(4) @(title : '{i18n>BusinessPartner.businessPartnerOccupation}');
busPartNationality : String(3) @(title : '{i18n>BusinessPartner.busPartNationality}');
busPartMaritalStatus : String(1) @(title : '{i18n>BusinessPartner.busPartMaritalStatus}');
businessPartnerPrintFormat : String(1) @(title : '{i18n>BusinessPartner.businessPartnerPrintFormat}');
businessPartnerDataOriginType : String(4) @(title : '{i18n>BusinessPartner.businessPartnerDataOriginType}');
salutation : String(50) @(title : '{i18n>BusinessPartner.salutation}');

    to_BPCreditProfile : Composition of one BusinessPartnerCreditProfile
    on to_BPCreditProfile.objectID = $self.objectID and to_BPCreditProfile.businessPartner = $self.businessPartner;

    to_BPCreditSegment : Composition of many BusinessPartnerCreditSegment
    on to_BPCreditSegment.objectID = $self.objectID and to_BPCreditSegment.businessPartner = $self.businessPartner;

    to_BuPaIdentification : Composition of many BusinessPartnerIdentification
    on to_BuPaIdentification.objectID = $self.objectID and to_BuPaIdentification.businessPartner = $self.businessPartner;

    to_BuPaIndustry : Composition of many BusinessPartnerIndustry
    on to_BuPaIndustry.objectID = $self.objectID and to_BuPaIndustry.businessPartner = $self.businessPartner;

    to_BusinessPartnerAddress : Composition of many BusinessPartnerAddress
    on to_BusinessPartnerAddress.objectID = $self.objectID and to_BusinessPartnerAddress.businessPartner = $self.businessPartner;

    to_BusinessPartnerBank : Composition of many BusinessPartnerBank
    on to_BusinessPartnerBank.objectID = $self.objectID and to_BusinessPartnerBank.businessPartner = $self.businessPartner;

    to_BusinessPartnerCard : Composition of many BusinessPartnerCard
    on to_BusinessPartnerCard.objectID = $self.objectID and to_BusinessPartnerCard.businessPartner = $self.businessPartner;

    to_BusinessPartnerRelationship : Composition of many BusinessPartnerRelationship
    on to_BusinessPartnerRelationship.objectID = $self.objectID and to_BusinessPartnerRelationship.businessPartner = $self.businessPartner;

    to_BusinessPartnerRole : Composition of many BusinessPartnerRole
    on to_BusinessPartnerRole.objectID = $self.objectID and to_BusinessPartnerRole.businessPartner = $self.businessPartner;

    to_BusinessPartnerTax : Composition of many BusinessPartnerTax
    on to_BusinessPartnerTax.objectID = $self.objectID and to_BusinessPartnerTax.businessPartner = $self.businessPartner;

    to_Customer : Composition of one Customer
    on to_Customer.objectID = $self.objectID and to_Customer.customer = $self.businessPartner;

    to_Supplier : Composition of one Supplier
    on to_Supplier.objectID = $self.objectID and to_Supplier.supplier = $self.businessPartner;
}
